<?php

use Illuminate\Database\Seeder;

class DecorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msdekorasi_cake')->insert([
        	'dekorasi_cake_name'=> 'Beautiful Garden',
        	'dekorasi_image'	=> '1_topping.png',
        	'price'				=> '120000',
        	'calorie'			=> '74',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msdekorasi_cake')->insert([
        	'dekorasi_cake_name'=> 'Strawberry Blast',
        	'dekorasi_image'	=> '2_topping.png',
        	'price'				=> '137000',
        	'calorie'			=> '89',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msdekorasi_cake')->insert([
        	'dekorasi_cake_name'=> 'Berry Forest',
        	'dekorasi_image'	=> '3_topping.png',
        	'price'				=> '100000',
        	'calorie'			=> '34',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msdekorasi_cake')->insert([
        	'dekorasi_cake_name'=> 'Chocolate Forest',
        	'dekorasi_image'	=> '4_topping.png',
        	'price'				=> '184000',
        	'calorie'			=> '97',
        	'flag_populer'		=> '7',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msdekorasi_cake')->insert([
        	'dekorasi_cake_name'=> 'Oreo Truffle',
        	'dekorasi_image'	=> '5_topping.png',
        	'price'				=> '119000',
        	'calorie'			=> '121',
        	'flag_populer'		=> '7',
        	'flag_delete'		=> '0',
        ]);
    }
}
