<?php

use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //d22
        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Lapis Surabaya',
        	'size_cake'			=> 'd22',
        	'price'				=> '300000',
        	'calorie'			=> '238',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Rainbow Cake',
        	'size_cake'			=> 'd22',
        	'price'				=> '275000',
        	'calorie'			=> '364',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Classic Sponge Cake',
        	'size_cake'			=> 'd22',
        	'price'				=> '275000',
        	'calorie'			=> '297',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Chocolate Cake',
        	'size_cake'			=> 'd22',
        	'price'				=> '275000',
        	'calorie'			=> '328',
        	'flag_populer'		=> '7',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Red Velvet Cheese',
        	'size_cake'			=> 'd22',
        	'price'				=> '300000',
        	'calorie'			=> '447',
        	'flag_populer'		=> '8',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Opera Cake',
        	'size_cake'			=> 'd22',
        	'price'				=> '290000',
        	'calorie'			=> '324',
        	'flag_populer'		=> '6',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Blackforest',
        	'size_cake'			=> 'd22',
        	'price'				=> '280000',
        	'calorie'			=> '264',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Chiffon Cake',
        	'size_cake'			=> 'd22',
        	'price'				=> '260000',
        	'calorie'			=> '278',
        	'flag_populer'		=> '2',
        	'flag_delete'		=> '0',
        ]);

        //d18
        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Lapis Surabaya',
        	'size_cake'			=> 'd18',
        	'price'				=> '280000',
        	'calorie'			=> '217',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Rainbow Cake',
        	'size_cake'			=> 'd18',
        	'price'				=> '258000',
        	'calorie'			=> '337',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Classic Sponge Cake',
        	'size_cake'			=> 'd18',
        	'price'				=> '250000',
        	'calorie'			=> '275',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Chocolate Cake',
        	'size_cake'			=> 'd18',
        	'price'				=> '262000',
        	'calorie'			=> '304',
        	'flag_populer'		=> '7',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Red Velvet Cheese',
        	'size_cake'			=> 'd18',
        	'price'				=> '270000',
        	'calorie'			=> '429',
        	'flag_populer'		=> '8',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Opera Cake',
        	'size_cake'			=> 'd18',
        	'price'				=> '276000',
        	'calorie'			=> '298',
        	'flag_populer'		=> '6',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Blackforest',
        	'size_cake'			=> 'd18',
        	'price'				=> '268000',
        	'calorie'			=> '248',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msbase_cake')->insert([
        	'base_cake_name'	=> 'Chiffon Cake',
        	'size_cake'			=> 'd18',
        	'price'				=> '237000',
        	'calorie'			=> '259',
        	'flag_populer'		=> '2',
        	'flag_delete'		=> '0',
        ]);
    }
}
