<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //d22
        DB::table('msproduct')->insert([
        	'product_name'		=> 'Sweet Flower',
        	'product_image'		=> '1_final kue.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '520000',
        	'calorie'			=> '394',
        	'base_cake_id'		=> '1',
        	'cover_cake_id'		=> '1',
        	'dekorasi_cake_id'	=> '1',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Rainbow Berry',
        	'product_image'		=> '2_final kue.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '542000',
        	'calorie'			=> '544',
        	'base_cake_id'		=> '2',
        	'cover_cake_id'		=> '2',
        	'dekorasi_cake_id'	=> '2',
        	'flag_populer'		=> '1',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Berry Forest',
        	'product_image'		=> '3_final kue.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '488000',
        	'calorie'			=> '428',
        	'base_cake_id'		=> '3',
        	'cover_cake_id'		=> '3',
        	'dekorasi_cake_id'	=> '3',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Chocolate Heaven',
        	'product_image'		=> '4_final kue.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '601000',
        	'calorie'			=> '531',
        	'base_cake_id'		=> '4',
        	'cover_cake_id'		=> '4',
        	'dekorasi_cake_id'	=> '4',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Red Velvet Oreo',
        	'product_image'		=> '5_final kue.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '544000',
        	'calorie'			=> '50',
        	'base_cake_id'		=> '5',
        	'cover_cake_id'		=> '5',
        	'dekorasi_cake_id'	=> '5',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        //d18
        DB::table('msproduct')->insert([
        	'product_name'		=> 'Sweet Flower',
        	'product_image'		=> '1_final kue.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '495000',
        	'calorie'			=> '373',
        	'base_cake_id'		=> '9',
        	'cover_cake_id'		=> '6',
        	'dekorasi_cake_id'	=> '1',
        	'flag_populer'		=> '3',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Rainbow Berry',
        	'product_image'		=> '2_final kue.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '515000',
        	'calorie'			=> '517',
        	'base_cake_id'		=> '10',
        	'cover_cake_id'		=> '7',
        	'dekorasi_cake_id'	=> '2',
        	'flag_populer'		=> '1',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Berry Forest',
        	'product_image'		=> '3_final kue.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '460000',
        	'calorie'			=> '406',
        	'base_cake_id'		=> '11',
        	'cover_cake_id'		=> '8',
        	'dekorasi_cake_id'	=> '3',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Chocolate Heaven',
        	'product_image'		=> '4_final kue.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '584000',
        	'calorie'			=> '507',
        	'base_cake_id'		=> '12',
        	'cover_cake_id'		=> '9',
        	'dekorasi_cake_id'	=> '4',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('msproduct')->insert([
        	'product_name'		=> 'Red Velvet Oreo',
        	'product_image'		=> '5_final kue.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '529000',
        	'calorie'			=> '646',
        	'base_cake_id'		=> '13',
        	'cover_cake_id'		=> '10',
        	'dekorasi_cake_id'	=> '5',
        	'flag_populer'		=> '5',
        	'flag_delete'		=> '0',
        ]);
    }
}
