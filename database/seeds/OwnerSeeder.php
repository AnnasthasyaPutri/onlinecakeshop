<?php

use Illuminate\Database\Seeder;

class OwnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msuser')->insert([
        	'user_name'		=>	'Owner',
        	'user_email'	=>	'sweetCake@gmail.com',
        	'user_password'	=>	bcrypt('owners'),
        	'user_phone'	=>	'081236547738',
        	'point'	        =>	'0',
        	'role_name'		=>	'owner',
        ]);
    }
}
