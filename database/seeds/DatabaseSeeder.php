<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        // $this->call(InformationSeeder::class);
        $this->call(OwnerSeeder::class);
        $this->call(UserSeeder::class);
        // $this->call(VoucherSeeder::class);
        // $this->call(ProductSeeder::class);
        // $this->call(BaseSeeder::class);
        // $this->call(CoverSeeder::class);
        // $this->call(DecorSeeder::class);
        // $this->call(VoucherUserSeeder::class);
    }
}
