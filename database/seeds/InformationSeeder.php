<?php

use Illuminate\Database\Seeder;

class InformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msinformation')->insert([
        	'alamat'			 => 'Jl. Taman Gilimanuk Utara I RT.8/RW.12, Kalideres, Jakarta Barat, 11840',
        	'email'				 => 'sweetCake@gmail.com',
        	'contact_information'=> '081236547738',
            'instagram'          => '@sweetCakes',
            'facebook'           => 'Sweet Cakes',
            'twitter'            => '@sweetcakesid',
        ]);
    }
}
