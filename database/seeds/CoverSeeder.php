<?php

use Illuminate\Database\Seeder;

class CoverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //d22
        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'White Cream',
        	'cover_image'		=> '1_polos.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '100000',
        	'calorie'			=> '82',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Pastel Cream',
        	'cover_image'		=> '2_polos.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '130000',
        	'calorie'			=> '91',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Glaze',
        	'cover_image'		=> '3_polos.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '113000',
        	'calorie'			=> '97',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Chocolate Ganache',
        	'cover_image'		=> '4_polos.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '142000',
        	'calorie'			=> '106',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Red Velvet Garnish',
        	'cover_image'		=> '5_polos.png',
        	'size_cake'			=> 'd22',
        	'price'				=> '125000',
        	'calorie'			=> '96',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        //d18
        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'White Cream',
        	'cover_image'		=> '1_polos.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '95000',
        	'calorie'			=> '82',
        	'flag_populer'		=> '10',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Pastel Cream',
        	'cover_image'		=> '2_polos.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '120000',
        	'calorie'			=> '91',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Glaze',
        	'cover_image'		=> '3_polos.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '110000',
        	'calorie'			=> '97',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Chocolate Ganache',
        	'cover_image'		=> '4_polos.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '138000',
        	'calorie'			=> '106',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);

        DB::table('mscover_cake')->insert([
        	'cover_cake_name'	=> 'Red Velvet Garnish',
        	'cover_image'		=> '5_polos.png',
        	'size_cake'			=> 'd18',
        	'price'				=> '140000',
        	'calorie'			=> '96',
        	'flag_populer'		=> '4',
        	'flag_delete'		=> '0',
        ]);
    }
}
