<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msuser')->insert([
        	'user_name'		=> 'Thasya',
        	'user_email'	=> 'annasthasya.putri20@gmail.com',
        	'user_password'	=> bcrypt('123456'),
        	'user_phone'	=> '082260375521',
        	'point'			=> '0',
        	'role_name'		=> 'user',
        ]);
    }
}
