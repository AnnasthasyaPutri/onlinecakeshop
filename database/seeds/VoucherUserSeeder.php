<?php

use Illuminate\Database\Seeder;

class VoucherUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msvoucher_user')->insert([
        	'user_id'		=> '2',
        	'voucher_id'	=> '1',
        	'flag_delete'	=> '0',
        ]);

        DB::table('msvoucher_user')->insert([
        	'user_id'		=> '2',
        	'voucher_id'	=> '2',
        	'flag_delete'	=> '0',
        ]);
    }
}
