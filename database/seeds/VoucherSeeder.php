<?php

use Illuminate\Database\Seeder;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msvoucher')->insert([
        	'voucher_type'	=> '1',
        	'voucher_code'	=> 'Diskonlagi',
        	'voucher_name'	=> 'MayDay',
        	'voucher_detail'=> 'Pembelian total produk 500.000 mendapat potongan 25% maksimal 200.000',
        	'minimum'		=> '500000',
        	'discount'		=> '200000',
        	'percent'		=> '0.25',
        	'qty'			=> '50',
        	'start_date'	=> '2019-05-01',
        	'expired_date'	=> '2020-04-30',
        	'point'			=> '250',
        	'flag_delete'	=> '0',
        ]);

        DB::table('msvoucher')->insert([
        	'voucher_type'	=> '2',
        	'voucher_code'	=> 'BlackSweet',
        	'voucher_name'	=> 'BlackSweetCake',
        	'voucher_detail'=> 'Setiap Pembelian Chocolate Base Cake mendapatkan potongan 50.000',
        	'discount'		=> '50000',
        	'qty'			=> '20',
        	'start_date'	=> '2019-05-01',
        	'expired_date'	=> '2020-04-30',
        	'point'			=> '100',
        	'base_cake_id'	=> '4',
        	'flag_delete'	=> '0',
        ]);
    }
}
