<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsvoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msvoucher', function (Blueprint $table) {
            $table->increments('voucher_id');
            $table->tinyInteger('voucher_type');
            $table->string('voucher_code');
            $table->string('voucher_name');
            $table->string('voucher_detail');
            $table->integer('minimum')->nullable();
            $table->integer('discount')->nullable();
            $table->double('percent')->nullable();
            $table->integer('qty');
            $table->date('start_date');
            $table->date('expired_date');
            $table->integer('point');
            $table->tinyInteger('flag_delete');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
