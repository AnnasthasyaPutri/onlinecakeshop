<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPopuler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('msproduct', function (Blueprint $table) {
            $table->integer('flag_populer');
        });

        Schema::table('msbase_cake', function (Blueprint $table) {
            $table->integer('flag_populer');
        });

        Schema::table('mscover_cake', function (Blueprint $table) {
            $table->integer('flag_populer');
        });

        Schema::table('msdekorasi_cake', function (Blueprint $table) {
            $table->integer('flag_populer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
