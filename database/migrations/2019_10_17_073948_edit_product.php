<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('msproduct', function (Blueprint $table) {
            $table->dropcolumn('name');
            $table->dropcolumn('type_cake');
            $table->dropcolumn('cover_color_id');
            $table->string('product_name');
            $table->integer('cover_cake_id');
            $table->integer('dekorasi_cake_id')->nullable();
            $table->string('product_image');
        });

        Schema::drop('msproduct_detail');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
