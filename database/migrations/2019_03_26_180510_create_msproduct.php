<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsproduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msproduct', function (Blueprint $table) {
            $table->increments('product_id');
            $table->string('name');
            $table->integer('type_cake');
            $table->string('size_cake');
            $table->integer('price');
            $table->integer('calorie');
            $table->integer('base_cake_id');
            $table->integer('cover_color_id');
            $table->tinyInteger('flag_delete');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
