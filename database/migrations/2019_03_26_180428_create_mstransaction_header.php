<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstransactionHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mstransaction_header', function (Blueprint $table) {
            $table->increments('transaction_header_id');
            $table->integer('user_id');
            $table->string('user_name');
            $table->string('transaction_code');
            $table->integer('total_price');
            $table->tinyInteger('payment_status');
            $table->string('transaction_status');
            $table->date('pick_date');
            $table->integer('type_cake');
            $table->integer('base_cake_id');
            $table->integer('cover_color_id');
            $table->string('filename');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
