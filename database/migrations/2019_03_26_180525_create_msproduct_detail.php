<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsproductDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msproduct_detail', function (Blueprint $table) {
            $table->increments('product_detail_id');
            $table->integer('product_id');
            $table->string('name');
            $table->integer('dekorasi_cake_id');
            $table->integer('dekorasi_color_id');
            $table->string('posisi');
            $table->tinyInteger('flag_delete');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
