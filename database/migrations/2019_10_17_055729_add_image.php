<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mscover_cake', function (Blueprint $table) {
            $table->string('cover_image');
            $table->integer('calorie');
            $table->integer('price');
        });

        Schema::table('msdekorasi_cake', function (Blueprint $table) {
            $table->string('dekorasi_cake_name');
            $table->string('dekorasi_image');
            $table->integer('calorie');
            $table->integer('price');
            $table->dropColumn('dekorasi_cake_material');
            $table->dropColumn('keterangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
