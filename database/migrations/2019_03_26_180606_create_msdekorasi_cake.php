<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsdekorasiCake extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msdekorasi_cake', function (Blueprint $table) {
            $table->increments('dekorasi_cake_id');
            $table->string('dekorasi_cake_material');
            $table->string('keterangan');
            $table->tinyInteger('flag_delete');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
