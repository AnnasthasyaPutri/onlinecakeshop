<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMscoverColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mscover_color', function (Blueprint $table) {
            $table->increments('cover_color_id');
            $table->integer('cover_cake_id');
            $table->string('color');
            $table->integer('price');
            $table->integer('calorie');
            $table->tinyInteger('flag_delete');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
