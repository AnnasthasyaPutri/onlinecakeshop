<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsdataPembayaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msdata_pembayaran', function (Blueprint $table) {
            $table->increments('data_pembayaran_id');
            $table->integer('user_id');
            $table->integer('transaction_header_id');
            $table->date('tanggal_pembayaran');
            $table->string('jumlah_pembayaran');
            $table->string('bank_asal');
            $table->string('nomer_rekening');
            $table->string('atas_nama');
            $table->string('filename');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
