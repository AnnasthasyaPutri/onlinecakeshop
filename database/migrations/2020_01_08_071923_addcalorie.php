<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addcalorie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mstransaction_header', function (Blueprint $table) {
            $table->dropColumn('total_calory');
        });

        Schema::table('mstransaction_detail', function (Blueprint $table) {
            $table->integer('price');
            $table->integer('calorie');
            $table->integer('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
