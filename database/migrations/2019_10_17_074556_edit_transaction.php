<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mstransaction_header', function (Blueprint $table) {
            $table->dropcolumn('type_cake');
            $table->dropcolumn('base_cake_id');
            $table->dropcolumn('cover_color_id');
            $table->dropcolumn('filename');
        });

        Schema::table('mstransaction_detail', function (Blueprint $table) {
            $table->dropcolumn('dekorasi_color_id');
            $table->dropcolumn('posisi');
            $table->integer('base_cake_id');
            $table->integer('cover_cake_id');
            $table->integer('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
