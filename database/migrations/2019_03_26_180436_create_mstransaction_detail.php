<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMstransactionDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mstransaction_detail', function (Blueprint $table) {
            $table->increments('transaction_detail_id');
            $table->integer('transaction_header_id');
            $table->integer('dekorasi_cake_id')->nullable();
            $table->integer('dekorasi_color_id');
            $table->string('posisi');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
