<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('msvoucher', function (Blueprint $table) {
            $table->dropcolumn('product');
            $table->dropcolumn('base');
            $table->dropcolumn('cover');
            $table->dropcolumn('decor');
            $table->integer('product_id')->nullable();
            $table->integer('base_cake_id')->nullable();
            $table->integer('cover_cake_id')->nullable();
            $table->integer('decor_cake_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
