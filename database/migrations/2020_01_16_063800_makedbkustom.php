<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Makedbkustom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mskustom', function (Blueprint $table) {
            $table->increments('kustom_id');
            $table->string('size_cake');
            $table->integer('price');
            $table->integer('calorie');
            $table->integer('base_cake_id');
            $table->integer('cover_color_id');
            $table->tinyInteger('flag_delete');
            $table->string('image');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
