<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsvoucherUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msvoucher_user', function (Blueprint $table) {
            $table->increments('voucher_user_id');
            $table->integer('user_id');
            $table->integer('voucher_id');
            $table->date('expired_date');
            $table->tinyInteger('flag_delete');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
