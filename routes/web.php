<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//system
Route::get('/login', 'System\LoginController@index');
Route::post('/login', 'System\LoginController@login');
Route::post('/register', 'System\RegisterController@register');
Route::post('/forgotpassword','System\ForgotPasswordController@forgotemail');
Route::get('/forgot/{token}','System\ForgotPasswordController@forgotview');
Route::post('/changepassword','System\ForgotPasswordController@changepassword');
Route::get('/logout', 'System\LoginController@logout');
Route::post('/filterdiameter22', 'System\FilterController@filterd22');
Route::post('/filterdiameter18', 'System\FilterController@filterd18');
Route::get('/compareresultsfromcustom/{kustom}/{id}','System\CompareController@comparecustom');
Route::get('/compareresultsfromproduct/{product}/{id}','System\CompareController@compareproduct');
Route::get('/choosecomparefromcustom/{kustom}','System\CompareController@choosecomparecustom');
Route::get('/choosecomparefromproduct/{product}','System\CompareController@choosecompareproduct');
Route::get('/custom/{id}','System\CustomController@viewcustom');
Route::post('/resultcustom','System\CustomController@custom');
//Transaction
// Route::get('/home', 'System\HomeController@index');
Route::get('/productdiameter22', 'User\ProductController@viewd22');
Route::get('/productdiameter18', 'User\ProductController@viewd18');
Route::get('/viewdetailproduct/{id}', 'User\ProductController@viewdetailproduct');
Route::get('/viewinformation','User\InformationController@index');
Route::get('/paymentconfirmation','User\PaymentConfirmationController@view');
Route::post('/paymentconfirmation','User\PaymentConfirmationController@confirmation');
Route::post('/store/','User\CartController@store');
Route::get('/cart','User\CartController@index');
Route::post('/cart','User\CartController@update');
Route::post('/updatenote','User\CartController@updatenote');
Route::delete('destroy/{id}','User\CartController@destroy');
Route::delete('emptyCart','User\CartController@emptyCart');
Route::get('/viewcheckout','User\CheckoutController@index');
Route::post('/checkout','User\CheckoutController@store');

//Owner
Route::get('/information', 'Owner\InformationController@index');
Route::get('/ubahinfo','Owner\InformationController@ubahprofile');
Route::post('/ubahinfo','Owner\InformationController@ubah');
Route::post('/ubahsandi','Owner\InformationController@ubahsandi');
//product
Route::get('/listproduct','Owner\ProductController@index');
Route::get('/viewaddproductform','Owner\ProductController@viewadd');
Route::post('/addproduct','Owner\ProductController@addproduct');
Route::get('/viewupdateproductform/{id}','Owner\ProductController@viewupdate');
Route::post('/updateproduct','Owner\ProductController@updateproduct');
Route::post('/deleteproduct/{id}','Owner\ProductController@deleteproduct');
//base
Route::get('/listbasecake','Owner\BaseController@index');
Route::get('/viewaddbaseform','Owner\BaseController@viewadd');
Route::post('/addbasecake','Owner\BaseController@addbasecake');
Route::get('/viewupdatebaseform/{id}','Owner\BaseController@viewupdate');
Route::post('/updatebasecake','Owner\BaseController@updatebasecake');
Route::post('/deletebasecake/{id}','Owner\BaseController@deletebasecake');
//cover
Route::get('/listcovercake','Owner\CoverController@index');
Route::get('/viewaddcoverform','Owner\CoverController@viewadd');
Route::post('/addcovercake','Owner\CoverController@addcovercake');
Route::get('/viewupdatecoverform/{id}','Owner\CoverController@viewupdate');
Route::post('/updatecovercake','Owner\CoverController@updatecovercake');
Route::post('/deletecovercake/{id}','Owner\CoverController@deletecovercake');
//dekor
Route::get('/listdecorcake','Owner\DekorController@index');
Route::get('/viewadddecorationform','Owner\DekorController@viewadddecor');
Route::post('/adddecorcake','Owner\DekorController@adddecorationcake');
Route::get('/viewupdatedecorationform/{id}','Owner\DekorController@viewupdatedecor');
Route::post('/updatedecorcake','Owner\DekorController@updatedecorationcake');
Route::post('/deletedecorationcake/{id}','Owner\DekorController@deletedecorcake');
//order
Route::get('/listorder','Owner\OrderController@index');
Route::get('/orderdetail/{id}','Owner\OrderController@orderdetail');
Route::get('/viewbuyer/{id}','Owner\OrderController@viewbuyer');
Route::get('/rememberpayment/{id}','Owner\OrderController@rememberpayment');
Route::get('/viewpaymentconfirmation/{id}','Owner\OrderController@viewconfirmation');
Route::post('/approvedpayment','Owner\OrderController@approved');
Route::post('/updatepickorder','Owner\OrderController@update');
Route::get('/approvedpickup/{id}','Owner\OrderController@pickup');
Route::get('/cancelorder/{id}','Owner\OrderController@cancel');
Route::get('/downloadpaymentconfirmation/{id}','Owner\OrderController@downloadfile');
//user
Route::get('/listdatauser','Owner\UserController@index');
Route::post('/sendvoucher/{id}','Owner\UserController@sendvoucher');
//voucher
Route::get('/listvoucher','Owner\VoucherController@index');
Route::get('/viewaddvoucherform','Owner\VoucherController@viewadd');
Route::post('/addvoucher','Owner\VoucherController@addvoucher');
Route::get('/updatevoucher/{id}','Owner\VoucherController@viewupdate');
Route::post('/updatevoucher','Owner\VoucherController@updatevoucher');
Route::post('/deletevoucher/{id}','Owner\VoucherController@delete');

//User
Route::get('/profile','User\UserProfileController@index');
Route::get('/ubahprofile','User\UserProfileController@ubahprofile');
Route::post('/ubahprofile','User\UserProfileController@ubah');
Route::post('/ubahpassword','User\UserProfileController@ubahsandi');
Route::get('/addvoucher/{id}','User\YourVoucherController@viewaddvoucher');
Route::post('/usevoucher','User\YourVoucherController@usevoucher');
Route::get('/unusevoucher','User\YourVoucherController@unusevoucher');
Route::get('/historyorder','User\HistoryOrderController@index');
Route::get('/historyorderdetail/{id}','User\HistoryOrderController@viewdetail');
Route::get('/yourvoucher','User\YourVoucherController@index');
Route::get('/reedemvoucher','User\YourVoucherController@viewreedem');
Route::get('/reedemvoucher/{id}','User\YourVoucherController@reedem');
Route::get('/viewnotif','User\NotifController@index');
Route::get('/deletenotif/{id}','User\NotifController@delete');
Route::get('/emptynotif','User\NotifController@empty');
