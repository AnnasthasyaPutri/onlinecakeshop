<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/icon" href="{{asset('image/Logo.png')}}">

    <title>Sweet Cake</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('fontawesome-free-5.8.1-web/css/all.css')}}">
	<link rel="stylesheet" href="{{asset('bootstrap-4.3.1-dist/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	



</head>
<body>
	<header id="page_header">
		@if(Auth::guest())
		<div id="top-bar" class="row">
			<div class="col-lg-7 col-md-7 logo-bar">
				<img class="big-logo" src="{{asset('image/mini_logo.png')}}">
			</div>
			<div class="col-lg-5 col-md-5 right-bar">
				<div class="bar">
					<span class="dropdown">
						<a class="dropmenu" data-toggle="dropdown-content" aria-haspopup="true" aria-expanded="false">
			              <i class="fas fa-user"></i>
			              <i class="fa fa-caret-down"></i>
			            </a>
			            <div class="dropdown-content" aria-labelledby="dropmenu">
			            	<a class="dropdown-item" href="{{url('/login')}}">Login</a>
			            	<a class="dropdown-item" href="{{url('/paymentconfirmation')}}">Konfirmasi Pembayaran</a>
			            </div>
					</span>&nbsp;
					<span class="cart">
						<a href="{{url('/cart')}}">
				        	<i class="fas fa-shopping-bag"></i>
				        	<span class="badge count iconbar-message-count">{{Count(Cart::content())}}</span>
				        </a>
					</span>
				</div>
			</div>
		</div>
		<section id="menu">
			<div class="header-menu">
				<a href="{{url('/productdiameter22')}}">Kue D22</a>&nbsp;
				<a href="{{url('/productdiameter18')}}">Kue D18</a>&nbsp;
				<a href="{{url('/viewinformation')}}">Informasi</a>
			</div>
		</section>

		@elseif(Auth::User()->role_name == 'user')
		<div id="top-bar" class="row">
			<div class="col-lg-7 col-md-7 logo-bar">
				<img class="big-logo" src="{{asset('image/mini_logo.png')}}">
			</div>
			<div class="col-lg-5 col-md-5 right-bar">
				<div class="bar">
					<span class="poin" style="font-size: 15px;margin-right: 5px;">
						<b>Your Point: {{Auth::User()->point}}</b>
					</span>&nbsp;
					<span class="dropdown">
						<a class="dropmenu" data-toggle="dropdown-content" aria-haspopup="true" aria-expanded="false">
			              <i class="fas fa-user"></i>
			              <i class="fa fa-caret-down"></i>
			            </a>
			            <div class="dropdown-content" aria-labelledby="dropmenu">
			            	<a class="dropdown-item" href="{{url('/profile')}}">Profile</a>
			            	<a class="dropdown-item" href="{{url('/paymentconfirmation')}}">Konfirmasi Pembayaran</a>
			            	<a class="dropdown-item" href="{{url('/historyorder')}}">Riwayat Pesanan</a>
			            	<a class="dropdown-item" href="{{url('/yourvoucher')}}">Kupon</a>
			            	<a class="dropdown-item" href="{{url('/logout')}}">Logout</a>
			            </div>
					</span>&nbsp;
					<span class="notif">
						<a href="{{url('/viewnotif')}}">
				        	<i class="fa fa-bell"></i>
				        	<span class="badge count iconbar-message-count">{{Count(Auth::User()->notif)}}</span>
				        </a>
					</span>
					<span class="cart">
						<a href="{{url('/cart')}}">
				        	<i class="fas fa-shopping-bag"></i>
				        	<span class="badge count iconbar-message-count">{{Count(Cart::content())}}</span>
				        </a>
					</span>
				</div>
			</div>
		</div>
		<section id="menu">
			<div class="header-menu">
				<a href="{{url('/productdiameter22')}}">Kue D22</a>&nbsp;
				<a href="{{url('/productdiameter18')}}">Kue D18</a>&nbsp;
				<a href="{{url('/viewinformation')}}">Informasi</a>
			</div>
		</section>

		@elseif(Auth::User()->role_name == 'owner')
		<div id="top-bar" class="row">
			<div class="col-lg-7 col-md-7 logo-bar">
				<img class="big-logo" src="{{asset('image/mini_logo.png')}}">
			</div>
			<div class="col-lg-5 col-md-5 right-bar" style="padding-right: 140px;">
				<div class="bar">
					<span class="dropdown">
						<a class="dropmenu" data-toggle="dropdown-content" aria-haspopup="true" aria-expanded="false">
			              <i class="fas fa-user"></i>
			              <i class="fa fa-caret-down"></i>
			            </a>
			            <div class="dropdown-content" aria-labelledby="dropmenu">
			            	<a class="dropdown-item" href="{{('/information')}}">Detil Informasi</a>
			            	<a class="dropdown-item" href="{{url('/logout')}}">Logout</a>
			            </div>
					</span>
				</div>
			</div>
		</div>
		<section id="menu">
			<div class="header-menu">
				<a href="{{url('/listproduct')}}">Produk</a>&nbsp;
				<a href="{{url('/listbasecake')}}">Kue Dasar</a>&nbsp;
				<a href="{{url('/listcovercake')}}">Lapisan Kue</a>&nbsp;
				<a href="{{url('/listdecorcake')}}">Dekorasi Kue</a>&nbsp;
				<a href="{{url('/listorder')}}">Pesanan</a>&nbsp;
				<a href="{{url('/listvoucher')}}">Kupon</a>&nbsp;
				<a href="{{url('/listdatauser')}}">User</a>
			</div>
		</section>
		@endif
	</header>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	@section('content')
      <!--Munculin isi content di sini-->
     @show

    <footer>
       <div class="footer">
         copyright &copy;2020 SweetCake
       </div>
   	</footer>
</body>