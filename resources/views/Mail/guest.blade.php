<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sweet Cake</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="icon" href="{{URL::asset('image/Logo.png')}}">
</head>


<body  style="margin: 0px 0px 0px 0px; padding: 0px 0px 0px 0px; font-family: Courier New, monospace;">

<table width="100%" height="100%" style="padding: 20px 0px 20px 0px;">
  <tr  align="center">
    <td>
    <!--Start Header-->
    <table width="562" cellpadding="0" cellspacing="0" style="color:#20202; font-weight: bold; padding: 16px 0px 16px 14px; font-family: Courier New, monospace; background-color: transparent;">
      <tr>
        <td>
          <img src="http://127.0.0.1:8000/image/Logo.png" height="100" width="100" style="" >
        </td>
      </tr>
    </table>
    <!--end header-->


<!--Start Ribbon-->
<table cellpadding="0" cellspacing="0" width="562">
  <tr>
    <td width="300" height="203" style="font-family: Courier New, monospace; padding: 10px 25px 0px 15px; font-size: 12px; color:#20202; ">
    <span style="text-transform: uppercase;font-size: 30px; font-weight: bold; color: #ff4a83"> Sweet Cake</span><br><br>
    <span style="width: 525px; ">
   Hi, {{$status}}</span><br><br>
    </td>
  </tr>
</table>

    </td>
  </tr>
</table>

</body>
</html>
