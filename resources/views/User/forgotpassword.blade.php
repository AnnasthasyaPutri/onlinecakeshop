@extends('layout.layoutuser')
@section('content')

<div class="container-fluid" style="margin-bottom: 8%;">
    <form class="form-horizontal" action="{{url('/changepassword')}}" method="post">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Ubah Kata Sandi</h3>
        </div>
        <input type="hidden" name="token" value="{{$data}}">

        <div class="container">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Email</label>
                <input type="email" name="email" class="form-control col-md-6 col-sm-6" value="{{old('email')}}" required>
                @if($errors->first('email'))<div class="alert alert-danger">{{$errors->first('email')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kata Sandi Baru</label>
                <input type="password" name="password" class="form-control col-md-6 col-sm-6" value="{{old('password')}}" required>
                @if($errors->first('password'))<div class="alert alert-danger">{{$errors->first('password')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Konfirmasi Kata Sandi</label>
                <input type="password" name="confirm_password" class="form-control col-md-6 col-sm-6" value="{{old('confirm_password')}}" required>
                @if($errors->first('confirm_password'))<div class="alert alert-danger">{{$errors->first('confirm_password')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class">
            <input type="submit" class="btn btn-md btn-submit" value="Ubah Kata Sandi">
        </div>
    </form>
</div>
@endsection