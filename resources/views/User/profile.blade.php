@extends('layout.layoutuser')
@section('content')
<div class="container-fluid profile-page">
    <div class="y-title">
        <h2>Profile</h2>
    </div>
    <div class="container profile row">
        <h5 class="col-lg-4 col-md-4 col-sm-6">Nama</h5>
        <h5 class="col-lg-8 col-md-8 col-sm-6">{{$user->user_name}}</h5><br>
        <h5 class="col-lg-4 col-md-4 col-sm-6">Email</h5>
        <h5 class="col-lg-8 col-md-8 col-sm-6">{{$user->user_email}}</h5><br>
        <h5 class="col-lg-4 col-md-4 col-sm-6">Nomor HP</h5>
        <h5 class="col-lg-8 col-md-8 col-sm-6">{{$user->user_phone}}</h5><br>
        <h5 class="col-lg-4 col-md-4 col-sm-6">Poin</h5>
        <h5 class="col-lg-8 col-md-8 col-sm-6">{{$user->point}}</h5><br>
        <a href="{{url('/')}}" style="color: #636b6f; text-decoration: none;" class="col-lg-4 col-md-4 col-sm-6"><h5>Kupon</h5></a>
        <h5 class="col-lg-8 col-md-8 col-sm-6">{{Count($user->voucher)}}</h5><br>
        <a href="{{url('/')}}" style="color: #636b6f; text-decoration: none;" class="col-lg-4 col-md-4 col-sm-6"><h5 >Pesanan</h5></a>
        <h5 class="col-lg-8 col-md-8 col-sm-6">{{Count($user->transaction)}}</h5><br><br>
        <div class="btn-class">
            <a href="{{url('/ubahprofile')}}" class="btn btn-md btn-submit">Ubah</a>
        </div>
    </div>
</div>
@endsection