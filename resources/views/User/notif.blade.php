@extends('layout.layoutuser')
@section('content')

<div class="container-fluid" style="margin-bottom: 11%">
    <div class="y-title">
        <h3>Notifikasi</h3>
    </div>
    <div class="container">
        <div class="row">
            @if(sizeof($notifs) == 0)
            <div class="container" style="margin-bottom: 50px;">
                <h4>Saat ini belum ada notifikasi</h4><br>
            </div>
            <span><a href="{{url('/productdiameter22')}}" class="btn btn-submit btn-lg" style="float: right;">Kembali</a></span>
            @else
            <table class="table dataTabel">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Subject</th>
                        <th>Keterangan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0; ?>
                    @foreach($notifs as $notif)
                    <tr>
                        <td>{{\Carbon\Carbon::parse($notif->tanggal)->format('d-m-Y')}}</td>
                        <td>{{$notif->subject}}</td>
                        <td>{{$notif->keterangan}}</td>
                        <td><a href="{{url('/deletenotif/'.Crypt::encrypt($notif->notif_id))}}"><span class="text-danger" style="cursor: pointer;">Hapus</span></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <span><a href="{{url('/emptynotif')}}" class="btn btn-danger btn-lg" style="float: right; margin-top: 20px;">Kosongkan</a></span>
            @endif
        </div>
    </div>
</div>
@endsection