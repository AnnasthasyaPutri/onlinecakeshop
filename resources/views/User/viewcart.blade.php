@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
  @if (session('statusok'))
  <div class="alert alert-success">
      Kue sudah masuk ke keranjang! 
  </div>
  @endif

  @if (session('statushapus'))
  <div class="alert alert-success">
      Kue sudah di hapus! 
  </div>
  @endif

  <h1>Keranjang</h1>
  <hr>

  @if(sizeof(Cart::content()) == 0)
    <h3 style="margin-bottom: 20%;">Keranjangmu Kosong. Ayo Belanja</h3>
	@else
	<div class="cart-table dataTable">
		<table class="cart">
			<tbody>
				@foreach(Cart::content() as $item)
				<tr>
					<td><img src="{{$item->options->image}}" alt="item" class="cart-table-img"></td>
					<td><h5>{{$item->name}}</h5><p>{{$item->options->base_name}}, {{$item->options->cover_name}}, {{$item->options->dekor_name}}, {{$item->options->size}}, {{$item->options->calorie}} kkal/potong</p><p>Note: <input type="text" name="note" class="note" data-id="{{$item->rowId}}" value="{{$item->options->note}}"></p></td>
					<td>Rp. <?php echo number_format($item->price, 0, ",", "."); ?></td>
					<td><input type="number" class="qtywidth" data-id="{{$item->rowId}}" name="qty" value="{{$item->qty}}" style="width: 20%;" min="1" required></td>
					<td><button type="button" data-url="{{url('/destroy',[$item->rowId])}}" class="btn btn-danger btn-hapus" id="{{$item->rowId}}" onclick="document.getElementById('hapus').style.display = 'block'">Hapus</button></td>
				</tr>
				@endforeach
			</tbody>
			<tfoot>
  				<tr>
  					<td colspan="3" style="text-align: right;"><h5>Total</h5></td>
  					<td colspan="4" id="total" style="text-align: right;"><h5>Rp. {{Cart::subtotal()}}</h5></td>
  				</tr>
			</tfoot>
		</table>

	<span><a href="{{url('/viewcheckout')}}" class="btn btn-success btn-lg" style="float: right; margin-right: 20px;">Lanjutkan</a></span>
	<span><button type="button" class="btn btn-danger btn-lg" onclick="document.getElementById('alert').style.display = 'block'" style="float: right; margin-right: 20px;">Kosongkan</button></span>&nbsp;
	
  @endif
</div>

<div id="hapus" class="modal">
    <form class="modal-content animate modal-hapus" method="POST" style="width: 420px; margin-top: 10%; margin-left: 33%;">
      {{csrf_field()}}
      <div class="imgcontainer">
        <span data-dismiss="modal" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
      </div>

      <br><br>
      <div class="container">
        <p style="text-align: center;">Yakin akan hapus kue ini?</p>
      </div>

      <br><br>
      <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
        <button type="button"  data-dismiss="modal" class="btn btn-danger btn-lg">Batal</button>
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit" class="btn btn-primary btn-lg" value="Setuju">
      </div>
    </form>
  </div>
</div>

<div id="alert" class="modal">

    <form class="modal-content animate" action="{{url('/emptyCart')}}" method="post" style="width: 420px; margin-top: 10%; margin-left: 33%;">
      {{csrf_field()}}
      <div class="imgcontainer">
        <span onclick="document.getElementById('alert').style.display='none'" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
      </div>

      <br><br>
      <div class="container">
        <p style="text-align: center;">Yakin kamu kosongkan keranjangmu?</p>
      </div>

      <br><br>
      <div class="container" style="background-color:#f1f1f1;margin:0 auto !important; text-align: center; padding:10px;" >
        <button type="button"  onclick="document.getElementById('alert').style.display='none'" class="btn btn-danger btn-lg">Batal</button>
        <input type="hidden" name="_method" value="DELETE">
		<input type="submit" class="btn btn-primary btn-lg" value="Setuju">
      </div>
    </form>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
      $('.dataTable').on("click", ".btn-hapus", function(){
          $('.modal-hapus').attr('action', '');

          var url = $(this).attr('data-url');
          $('.modal-hapus').attr('action', url);

      });

        $('.note').change(function(e){
        	var id = $(this).attr('data-id');
        	var note = $('.note[data-id="'+id+'"]').val();
        	console.log(id, note);
        	e.preventDefault();

        	$.ajax({
        		url: '{{url("/updatenote")}}',
        		type: "POST",
        		data:{
        			"_token": "{{ csrf_token() }}",
        			'id': id,
        			'note': note,
        		},
        		success: function(data){
        			if(data.success == true){
        				$('.note[data-id="'+data.rowid+'"]').attr('value',data.note);
        				console.log(data.message,data.note, data.rowid);
                location.reload();
        			}
        			else{
        				console.log('gagal');
                location.reload();
        			}
        		}
        	});
        	e.preventDefault();
        });

        $('.qtywidth').change(function(e){
          var id = $(this).attr('data-id');
          var qty = parseInt($('.qtywidth[data-id="'+id+'"]').val());
          console.log(id, qty);
          e.preventDefault();

          $.ajax({
            url: '{{url("/cart")}}',
            type: "post",
            data:{
              "_token": "{{ csrf_token() }}",
              'id': id,
              'qty': qty,
            },
            success: function(data){
              if(data.success == true){
                $('.qtywidth[data-id="'+data.rowid+'"]').attr('value',data.qty);
                console.log(data.qty, data.rowid);
                location.reload();
              }
              else{
                console.log('gagal');
                location.reload();
              }
            }
          });
          e.preventDefault();
        });
    });
</script>
@endsection