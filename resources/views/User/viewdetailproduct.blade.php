@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <form class="form-horizontal" action="/store" method="post">
        {{csrf_field()}}
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <img class="img-thumbnail" src="../product/{{$product->product_image}}" style=" margin-left: 40px; width: 75%; height: 460px; border: 3px solid #ff4a83;">
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <input type="hidden" name="product_id" value="{{$product->product_id }}">
                <div class="detail" style="margin-bottom: 50px;">
                    <h1 style="color: #ff4a83; margin-bottom: 20px;">{{$product->product_name}}</h1><br>
                    <h4>Detail</h4>
                    <input type="hidden" name="base" value="{{$product->base_cake_id}}">
                    <h5 style="margin-left: 20px;">Kue yang digunakan : {{$product->basecake->base_cake_name}}</h5>
                    <input type="hidden" name="cover" value="{{$product->cover_cake_id}}">
                    <h5 style="margin-left: 20px;">Lapisan yang digunakan : {{$product->covercake->cover_cake_name}}</h5>
                    <input type="hidden" name="decor" value="{{$product->dekorasi_cake_id}}">
                    <h5 style="margin-left: 20px;">Dekorasi yang digunakan : {{$product->dekorasicake->dekorasi_cake_name}}</h5>
                    <input type="hidden" name="calorie" value="{{$product->calorie}}">
                    <h5 style="margin-left: 20px;">Kalori : {{$product->calorie}} kkal/potong</h5><br>
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <h3 style="color: #ff4a83">Rp <?php echo number_format($product->price, 0, ",", "."); ?></h3><br>
                </div>
                <a href="{{url('/custom/'.Crypt::encrypt($product->product_id))}}" class="btn btn-primary" style="">Kustom</a>&nbsp;
                <a href="{{url('/choosecomparefromproduct/'.Crypt::encrypt($product->product_id))}}" class="btn btn-success" style="">Bandingkan</a>&nbsp;
                <button type="submit" class="btn btn-submit">Beli</button>
            </div>
        </div>
    </form>
</div>
@endsection