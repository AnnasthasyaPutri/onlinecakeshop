@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <form class="form-horizontal" action="/store" method="post">
        {{csrf_field()}}
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <img class="img-thumbnail" id="gambar" src="{{URL::asset('/custom/'.$kustoms->image)}}" style=" margin-left: 40px; width: 75%; height: 460px; border: 3px solid #ff4a83;">
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <input type="hidden" name="kustomId" id="kustomId" value="{{$kustoms->kustom_id}}">
                <div class="detail" style="margin-bottom: 50px;">
                    <input type="hidden" name="size" value="{{$kustoms->size_cake}}">
                    <div class="input-group">
                        <label class="col-md-3 col-sm-6">Ukuran</label>
                        @if($kustoms->size_cake == "d22")
                        <label class="col-md-6 col-sm-6">Diameter 22</label>
                        @else
                        <label class="col-md-6 col-sm-6">Diameter 18</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <label class="col-md-3 col-sm-6">Kue Dasar</label>
                        <select class="form-control col-md-6 col-sm-6" required name="base" id="base">
                            <option value="{{$kustoms->base_cake_id}}">{{$basecake->base_cake_name}}</option>
                            @foreach($bases as $bc)
                            <option value="{{$bc->base_cake_id}}">{{$bc->base_cake_name}}</option>
                            @endforeach
                        </select>
                        @if($errors->first('base'))<br><div class="alert alert-danger">{{$errors->first('base')}}</div>@endif
                    </div>
                    <div class="input-group">
                        <label class="col-md-3 col-sm-6">Lapisan Kue</label>
                        <select class="form-control col-md-6 col-sm-6" required name="cover" id="cover">
                            <option value="{{$kustoms->cover_cake_id}}"><img src="../cover/{{$covercake->cover_image}}" style="width: 50px;">{{$covercake->cover_cake_name}}</option>
                            @foreach($covers as $cc)
                            <option value="{{$cc->cover_cake_id}}">{{$cc->cover_cake_name}}</option>
                            @endforeach
                        </select>
                        @if($errors->first('cover'))<br><div class="alert alert-danger">{{$errors->first('cover')}}</div>@endif
                    </div>
                    <div class="input-group">
                        <label class="col-md-3 col-sm-6">Dekorasi Kue</label>
                        <select class="form-control col-md-6 col-sm-6" required name="dekor" id="dekor">
                            <option value="{{$kustoms->dekorasi_cake_id}}">{{$dekor->dekorasi_cake_name}}</option>
                            @foreach($dekors as $dc)
                            <option value="{{$dc->dekorasi_cake_id}}">{{$dc->dekorasi_cake_name}}</option>
                            @endforeach
                        </select>
                        @if($errors->first('dekor'))<br><div class="alert alert-danger">{{$errors->first('dekor')}}</div>@endif
                    </div>
                    <input type="hidden" name="calorie" value="{{$kustoms->calorie}}">
                    <h5 id="kalori" style="margin-left: 20px;">Kalori : {{$kustoms->calorie}} kkal/potong</h5><br>
                    <input type="hidden" name="price" value="{{$kustoms->price}}">
                    <h3 id="harga" style="color: #ff4a83">Rp <?php echo number_format($kustoms->price, 0, ",", "."); ?></h3><br>
                </div>
                <a href="{{url('/productdiameter22')}}" class="btn btn-danger" style="">Batal</a>&nbsp;
                <a href="{{url('/choosecomparefromcustom/'.Crypt::encrypt($kustoms->kustom_id))}}" class="btn btn-success" style="">Bandingkan</a>&nbsp;
                <button type="submit" class="btn btn-submit">Beli</button>
            </div>
        </div>
    </form>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#base').change(function(e){
            var id = $('#kustomId').val();
            var base = $('#base').val();
            var cover = $('#cover').val();
            var dekor = $('#dekor').val();
            console.log(id, base, cover, dekor);
            e.preventDefault();

            $.ajax({
                url: '{{url("/resultcustom")}}',
                type: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'id': id,
                    'base': base,
                    'cover': cover,
                    'dekor': dekor, 
                },
                success: function(data){
                    if(data.success == true){
                        console.log(data.message, data.price, data.kalori);
                        var harga = data.price;
                        var img = document.getElementById("gambar").getAttribute('src');
                        var timestamp = new Date().getTime();
                        document.getElementById("kalori").innerHTML = "Kalori : "+data.kalori+" kkal/potong";
                        document.getElementById("harga").innerHTML = "Rp "+data.price;
                        document.getElementById("gambar").src = img+"?t"+timestamp;
                    }
                    else{
                        console.log('gagal');
                    }
                }
            });
            e.preventDefault();
        });

        $('#cover').change(function(e){
            var id = $('#kustomId').val();
            var base = $('#base').val();
            var cover = $('#cover').val();
            var dekor = $('#dekor').val();
            console.log(id, base, cover, dekor);
            e.preventDefault();

            $.ajax({
                url: '{{url("/resultcustom")}}',
                type: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'id': id,
                    'base': base,
                    'cover': cover,
                    'dekor': dekor, 
                },
                success: function(data){
                    if(data.success == true){
                        console.log(data.message, data.price, data.kalori);
                        var harga = data.price;
                        var img = document.getElementById("gambar").getAttribute('src');
                        var timestamp = new Date().getTime();
                        document.getElementById("kalori").innerHTML = "Kalori : "+data.kalori+" kkal/potong";
                        document.getElementById("harga").innerHTML = "Rp "+data.price;
                        document.getElementById("gambar").src = img+"?t"+timestamp;
                    }
                    else{
                        console.log('gagal');
                    }
                }
            });
            e.preventDefault();
        });

        $('#dekor').change(function(e){
            var id = $('#kustomId').val();
            var base = $('#base').val();
            var cover = $('#cover').val();
            var dekor = $('#dekor').val();
            console.log(id, base, cover, dekor);
            e.preventDefault();

            $.ajax({
                url: '{{url("/resultcustom")}}',
                type: "POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'id': id,
                    'base': base,
                    'cover': cover,
                    'dekor': dekor, 
                },
                success: function(data){
                    if(data.success == true){
                        console.log(data.message, data.price, data.kalori);
                        var harga = data.price;
                        var img = document.getElementById("gambar").getAttribute('src');
                        var timestamp = new Date().getTime();
                        document.getElementById("kalori").innerHTML = "Kalori : "+data.kalori+" kkal/potong";
                        document.getElementById("harga").innerHTML = "Rp "+data.price;
                        document.getElementById("gambar").src = img+"?t"+timestamp;
                    }
                    else{
                        console.log('gagal');
                    }
                }
            });
            e.preventDefault();
        });
    });
</script>
@endsection