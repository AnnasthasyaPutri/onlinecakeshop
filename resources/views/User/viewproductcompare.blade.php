@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <div class="row">
        @foreach($products as $product)
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="img-product">
                <a href="{{url('/compareresultsfromproduct/'.Crypt::encrypt($prd->product_id).'/'.Crypt::encrypt($product->product_id))}}">
                    <img class="img-thumbnail" src="../product/{{$product->product_image}}" style="height: 300px; border-bottom: 3px solid #ff4a83;"><br>
                </a>
                @if($product->size_cake == 'd22')
                <h5 style="text-align: center; color: #ff4a83;">{{$product->product_name}} Diameter 22</h5>
                @else
                <h5 style="text-align: center; color: #ff4a83;">{{$product->product_name}} Diameter 18</h5>
                @endif
                <h5 style="text-align: center;">Rp. <?php echo number_format($product->price, 0, ",", "."); ?></h5>
                <h5 style="text-align: center;">Kalori : {{$product->calorie}} kkal/potong</h5>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection