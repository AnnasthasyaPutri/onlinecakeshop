<div class="container">
	@if(sizeof($vouchers) == 0)
    <div>
    	<span style="text-align: center; color: #ff4a83;"><h2><b>You don't have any voucher</b></h2></span>
    </div>
    @else
    <div class="voucher-table">
		<table class="voucher" style="border-collapse: collapse; width: 100%;">
			<thead>
				<tr>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">Name</td>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">Detail</td>
					<td style="border-bottom: 1px solid #ff4a83;"></td>
				</tr>
			</thead>
			<tbody>
				@foreach($vouchers as $voucher)
				@if($voucher->voucher_user_id == $vchr_id)
				<tr>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">{{$voucher->voucher->voucher_name}}</td>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">{{$voucher->voucher->voucher_detail}}</td>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">
						<label><h5 class="text-success">Used</h5></label>
						<a href="{{url('/unusevoucher')}}" class="text-danger" style="color: red; text-decoration: none;"><span style="cursor: pointer;" id="unusevoucher">Unused</span></a>
					</td>
				</tr>
				@else
				<tr>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">{{$voucher->voucher->voucher_name}}</td>
					<td style="text-align: left; border-bottom: 1px solid #f08080;">{{$voucher->voucher->voucher_detail}}</td>
					<td style="text-align: left; border-bottom: 1px solid #f08080;"><a href="{{url('/usevoucher/'.Crypt::encrypt($voucher->voucher_id))}}" style="color: green; text-decoration: none;"><span style="cursor: pointer;" id="usevoucher">Use</span></a></td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
	</div>
    @endif
</div>