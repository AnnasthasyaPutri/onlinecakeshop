@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12" style="border-right: 1px solid #ff4a83;">
            <form class="form-horizontal" action="/store" method="post">
                {{csrf_field()}}
                <img class="img-thumbnail" src="{{URL::asset('/custom/'.$image1)}}" style="margin-left: 20%; height: 300px; width: 300px; border: 2px solid #ff4a83;">
                <div class="detail" style="margin-left: 10%; margin-top: 2%;">
                    <input type="hidden" name="kustom_id" value="{{$kustom->kustom_id}}">
                    <input type="hidden" name="base" value="{{$kustom->base_cake_id}}">
                    <input type="hidden" name="cover" value="{{$kustom->cover_cake_id}}">
                    <input type="hidden" name="decor" value="{{$kustom->dekorasi_cake_id}}">
                    <input type="hidden" name="calorie" value="{{$kustom->calorie}}">
                    <input type="hidden" name="price" value="{{$kustom->price}}">
                    <h4>Kue Dasar : {{$base}}</h4>
                    <h4>Lapisan Kue : {{$cover}}</h4>
                    <h4>Dekorasi Kue : {{$decor}}</h4>
                    @if($kustom->size_cake == "d22")
                    <h4>Ukuran Kue : Diameter 22</h4>
                    @else
                    <h4>Ukuran Kue : Diameter 18</h4>
                    @endif
                    <h4>Kalori : {{$kustom->calorie}} kkal/potong</h4>
                    <h4>Harga : Rp. <?php echo number_format($kustom->price, 0, ",", "."); ?></h4><br>
                    <button type="submit" class="btn btn-submit">Beli</button>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <form class="form-horizontal" action="/store" method="post">
                {{csrf_field()}}
                <img class="img-thumbnail" src="{{URL::asset('/product/'.$image2)}}" style="margin-left: 20%; height: 300px; width: 300px; border: 2px solid #ff4a83;">
                <div class="detail" style="margin-left: 10%; margin-top: 2%;">
                    <input type="hidden" name="product_id" value="{{$product->product_id}}">
                    <input type="hidden" name="base" value="{{$product->base_cake_id}}">
                    <input type="hidden" name="cover" value="{{$product->cover_cake_id}}">
                    <input type="hidden" name="decor" value="{{$product->dekorasi_cake_id}}">
                    <input type="hidden" name="calorie" value="{{$product->calorie}}">
                    <input type="hidden" name="price" value="{{$product->price}}">
                    <h4>Kue Dasar : {{$product->basecake->base_cake_name}}</h4>
                    <h4>Lapisan Kue : {{$product->covercake->cover_cake_name}}</h4>
                    <h4>Dekorasi Kue : {{$product->dekorasicake->dekorasi_cake_name}}</h4>
                    @if($product->size_cake == "d22")
                    <h4>Ukuran Kue : Diameter 22</h4>
                    @else
                    <h4>Ukuran Kue : Diameter 18</h4>
                    @endif
                    <h4>Kalori : {{$product->calorie}} kkal/potong</h4>
                    <h4>Harga : Rp. <?php echo number_format($product->price, 0, ",", "."); ?></h4><br>
                    <button type="submit" class="btn btn-submit">Beli</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection