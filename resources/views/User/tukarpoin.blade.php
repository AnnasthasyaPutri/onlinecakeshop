@extends('layout.layoutuser')
@section('content')

@if(session('fail'))
    <div id="gagal_poin" class="modal" style="display: block;">
      <input type="hidden" name="_token" value="Od3l0ddhBWTJYCkvRfisFxqNFcV5LCuFb4zEXJBv">
        <!-- $errors->false -->
        <div class="container">
          <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" onclick="document.getElementById('gagal_poin').style.display = 'none'">&times;</button>Poin anda kurang</div>
        </div>
    </div>
@endif

<div class="container-fluid" style="margin-bottom: 11%">
    <div class="y-title">
        <h3>Tukar poin</h3>
    </div>
    <div class="container">
        <div class="row">
            @if(sizeof($vouchers) == 0)
            <div class="container" style="margin-bottom: 50px;">
                <h4>Saat ini kupon tidak tersedia</h4><br>
            </div>
            <span><a href="{{url('/yourvoucher')}}" class="btn btn-submit btn-lg" style="float: right;">Kembali</a></span>
            @else
            <table class="table dataTabel">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Detail Kupon</th>
                        <th>Batas Waktu</th>
                        <th>Poin</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0; ?>
                    @foreach($vouchers as $voucher)
                    <tr>
                        <td><?php $no = $no + 1; echo $no; ?></td>
                        <td>{{$voucher->voucher_name}}</td>
                        <td>{{$voucher->voucher_detail}}</td>
                        <td>{{\Carbon\Carbon::parse($voucher->expired_date)->format('d-m-Y')}}</td>
                        <td>{{$voucher->point}}</td>
                        <td><a href="{{url('/reedemvoucher/'.Crypt::encrypt($voucher->voucher_id))}}" style="color: green; text-decoration: none;"><span style="cursor: pointer;" id="reedem">Tukar poin</span></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>
@endsection