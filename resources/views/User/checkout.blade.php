@extends('layout.layoutuser')
@section('content')

@if(session('statusfail'))
<div id="datadatafail" class="modal">
    <form class="modal-content animate" action="{{ url('/checkout')}}" method="post" style="width: 70%; margin-top: 10%; margin-left: 13%;">
        {{csrf_field()}}

        <div class="imgcontainer">
            <span onclick="document.getElementById('datafail').style.display='none'" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
        </div><br>
        <div class="container">
            <input type="hidden" id="voucher" name="voucher" value="{{$vchr}}">
            <input type="hidden" id="total" name="price" value="{{$total}}">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                @if($errors->first('name'))<div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Email</label>
                <input type="email" name="email" class="form-control col-md-6 col-sm-6" value="{{old('email')}}" required>
                @if($errors->first('email'))<div class="alert alert-danger">{{$errors->first('email')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nomor HP</label>
                <input type="text" name="nomor_hp" class="form-control col-md-6 col-sm-6" value="{{old('nomor_hp')}}" required>
                @if($errors->first('nomor_hp'))<div class="alert alert-danger">{{$errors->first('nomor_hp')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Tanggal Pengambilan</label>
                <input type="date" name="tanggal" class="form-control col-md-6 col-sm-6" value="{{old('tanggal')}}" required>
                @if($errors->first('tanggal'))<br><div class="alert alert-danger">{{$errors->first('tanggal')}}</div>@endif
            </div>
        </div><br>

        <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
            <button type="button"  onclick="document.getElementById('datafail').style.display='none'" class="btn btn-danger btn-lg">Batal</button>
            <button type="button" class="btn btn-submit btn-lg">Pesan</button>
        </div>
    </form>
</div>
@endif

<div class="container-fluid">
    <div class="container" style="width: 95%; margin-top: 50px; margin-bottom: 6%; margin-left: 25px;">
        {{csrf_field()}}
        <div class="container" style="margin-top: 20px;">
            <div class="y-title" style="margin-bottom: 30px;">
                <h4>Pesanan Anda</h4>
            </div>
            <div class="cart-table">
                <table class="cart">
                    <thead>
                        <tr>
                            <td><b>Nama</b></td>
                            <td><b>Detail</b></td>
                            <td><b>Note</b></td>
                            <td><b>Qty</b></td>
                            <td><b>Harga</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(Cart::content() as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->options->base_name}}, {{$item->options->cover_name}}, {{$item->options->dekor_name}}, {{$item->options->size}}, {{$item->options->calorie}} cal/slice</td>
                            <td>{{$item->options->note}}</td>
                            <td>{{$item->qty}}</td>
                            <td>Rp. <?php echo number_format($item->price, 0, ",", "."); ?></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        @if(Auth::check()==true)
                        <tr>
                            <td colspan="3" style="text-align: right;"><h5>Potongan</h5></td>
                            <td colspan="3" id="potongan" style="text-align: right;"><h5>Rp. <?php echo number_format($potongan, 0, ",", "."); ?></h5></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right;"><h5>Total</h5></td>
                            <td colspan="3" id="price" style="text-align: right;"><h5>Rp. <?php echo number_format($total, 0, ",", "."); ?></h5></td>
                        </tr>
                        <tr>
                            <input type="hidden" id="voucher" name="voucher" value="{{$vchr}}">
                            <td colspan="4" style="text-align: right;"><h6 id="message" class="text-success">{{$message}}</h6></td>
                            <td colspan="3" style="text-align: right;"><h6 id="linkvoucher" style="color: #ff4a83; cursor: pointer;" onclick="document.getElementById('usevoucher').style.display='block'">Gunakan Kupon</h6></td>
                        </tr>
                        @else
                        <tr>
                            <td colspan="3" style="text-align: right;"><h5>Total</h5></td>
                            <td colspan="3" id="price" style="text-align: right;"><h5>Rp. <?php echo number_format($total, 0, ",", "."); ?></h5></td>
                        </tr>
                        @endif
                    </tfoot>
                </table>
            </div>
          </div>
          <div class="btn-grup" style="padding-left: 80%;">
            <a href="{{url('/cart')}}" class="btn btn-md btn-danger">Batal</a>&nbsp;
            <button type="button" class="btn btn-md btn-submit" onclick="document.getElementById('data').style.display = 'block'">Pesan</button>
        </div>
    </div>

    <div id="usevoucher" class="modal">
      <form class="modal-content animate modal-kirim" action="{{url('/usevoucher')}}" method="POST" style="width: 500px; margin-top: 10%; margin-left: 33%;">
        {{csrf_field()}}
        <div class="imgcontainer">
          <span onclick="document.getElementById('usevoucher').style.display = 'none'" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
        </div>

        <br><br>
        <div class="container">
            <p class="col-md-3 col-sm-3">Kupon</p>
            @if($vouchers == null)
            <p>Kamu tidak memiliki akses</p>
            @else
            @if(sizeof($vouchers)==0)
            <p>Kamu tidak memiliki kupon</p>
            @else
            <select class="col-md-9 col-sm-9 vchr" name="voucher" required >
                <option value="0">Batal gunakan</option>
                @foreach($vouchers as $voucher)
                <option value="{{$voucher->voucher_id}}">{{$voucher->voucher->voucher_name}}, {{$voucher->voucher->voucher_detail}}</option>
                @endforeach
            </select>
            @endif
            @endif
        </div>

        <br><br>
        <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
          <button type="button"  data-dismiss="modal" class="btn btn-danger btn-lg" onclick="document.getElementById('usevoucher').style.display = 'none'">Batal</button>
          <button type="submit" class="btn btn-primary btn-lg btn-approve">Setuju</button>
        </div>
      </form>
    </div>

    <div id="data" class="modal">
        <form class="modal-content animate" action="{{ url('/checkout')}}" method="post" style="width: 70%; margin-top: 10%; margin-left: 13%;">
            {{csrf_field()}}

            <div class="imgcontainer">
                <span onclick="document.getElementById('data').style.display='none'" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
            </div><br>
            <div class="container">
                <input type="hidden" id="voucher" name="voucher" value="{{$vchr}}">
                <input type="hidden" id="total" name="price" value="{{$total}}">
                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Nama</label>
                    <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                    @if($errors->first('name'))<div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
                </div>
                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Email</label>
                    <input type="email" name="email" class="form-control col-md-6 col-sm-6" value="{{old('email')}}" required>
                    @if($errors->first('email'))<div class="alert alert-danger">{{$errors->first('email')}}</div>@endif
                </div>
                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Nomor HP</label>
                    <input type="text" name="nomor_hp" class="form-control col-md-6 col-sm-6" value="{{old('nomor_hp')}}" required>
                    @if($errors->first('nomor_hp'))<div class="alert alert-danger">{{$errors->first('nomor_hp')}}</div>@endif
                </div>
                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Tanggal Pengambilan</label>
                    <input type="date" name="tanggal" class="form-control col-md-6 col-sm-6" value="{{old('tanggal')}}" required>
                    @if($errors->first('tanggal'))<br><div class="alert alert-danger">{{$errors->first('tanggal')}}</div>@endif
                </div>
            </div><br>

            <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
                <button type="button"  onclick="document.getElementById('data').style.display='none'" class="btn btn-danger btn-lg">Batal</button>
                <input type="submit" class="btn btn-primary btn-lg" value="Pesan">
            </div>

        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>

@endsection