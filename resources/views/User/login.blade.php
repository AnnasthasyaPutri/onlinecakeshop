@extends('layout.layoutuser')
@section('content')

@if(session('status'))
    <div id="id02" class="modal" style="display: block;">
      <input type="hidden" name="_token" value="Od3l0ddhBWTJYCkvRfisFxqNFcV5LCuFb4zEXJBv">
        <!-- $errors->false -->
        <div class="container">
          <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" onclick="document.getElementById('id02').style.display = 'none'">&times;</button>Silahkan lihat email anda</div>
        </div>
    </div>
@endif

@if(session('status1'))
    <div id="id03" class="modal" style="display: block;">
      <input type="hidden" name="_token" value="Od3l0ddhBWTJYCkvRfisFxqNFcV5LCuFb4zEXJBv">
        <!-- $errors->false -->
        <div class="container">
          <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" onclick="document.getElementById('id03').style.display = 'none'">&times;</button>Email anda tidak terdaftar</div>
        </div>
    </div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 login-form">
            <form class="form-horizontal" action="{{url('/login')}}" method="post">
                {{csrf_field()}}
                <div class="y-title">
                    <h3>Login</h3>
                </div>
                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Email</label>
                    <input type="email" name="email" class="form-control col-md-6 col-sm-6" value="{{old('email')}}" required>
                </div>

                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Kata Sandi</label>
                    <input type="password" name="password" class="form-control col-md-6 col-sm-6" value="{{old('password')}}" required>
                    @if($messageEmail == "")
                    @else
                      <br><div class="alert alert-danger"><em>{{$messageEmail}}</em></div>
                    @endif
                </div>

                <div class="input-group">
                    <span class="col-md-6 col-sm-6" id="forgot" style="cursor: pointer;" onclick="document.getElementById('id01').style.display = 'block'">Lupa Password?</span>
                </div>

                <div class="form-group button-class">
                    <input type="submit" class="btn btn-md btn-submit" value="Login">
                </div>
            </form>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 regist-form">
            <form class="form-horizontal" action="{{url('/register')}}" method="post">
                {{csrf_field()}}
                <div class="y-title">
                    <h3>Daftar</h3>
                </div>

                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Nama</label>
                    <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                    @if($errors->first('name'))<div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
                </div>

                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Email</label>
                    <input type="email" name="email" class="form-control col-md-6 col-sm-6" value="{{old('email')}}" required>
                    @if($errors->first('email'))<div class="alert alert-danger">{{$errors->first('email')}}</div>@endif
                </div>

                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Nomor HP</label>
                    <input type="text" name="phone" class="form-control col-md-6 col-sm-6" value="{{old('phone')}}" required>
                    @if($errors->first('phone'))<div class="alert alert-danger">{{$errors->first('phone')}}</div>@endif
                </div>

                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Kata Sandi</label>
                    <input type="password" name="password" class="form-control col-md-6 col-sm-6" value="{{old('password')}}" required>
                    @if($errors->first('password'))<div class="alert alert-danger">{{$errors->first('password')}}</div>@endif
                </div>

                <div class="input-group">
                    <label class="col-md-3 col-sm-6">Konfirmasi Kata Sandi</label>
                    <input type="password" name="password_confirmation" class="form-control col-md-6 col-sm-6" value="{{old('password_confirmation')}}" required>
                    @if($errors->first('password_confirmation'))<div class="alert alert-danger">{{$errors->first('password_confirmation')}}</div>@endif
                </div>

                <div class="form-group button-class">
                    <input type="submit" class="btn btn-md btn-submit" value="Daftar">
                </div>
            </form>
        </div>
    </div>

    <div id="id01" class="modal">
      <input type="hidden" name="_token" value="Od3l0ddhBWTJYCkvRfisFxqNFcV5LCuFb4zEXJBv">
        <form class="modal-content animate" action="{{url('/forgotpassword')}}" method="post">
          {{csrf_field()}}
          <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
          </div>

          <div class="container">
            <label for="user_email" class="col-lg-3 col-md-3 col-sm-3"><b>E-mail</b></label>
            <input type="email" class="col-lg-6 col-md-6 col-sm-6" placeholder="Masukan E-mail" name="user_email" required>
          </div>
          <div class="btn-modal">
              <button type="submit" class="btn btn-md btn-submit">Ubah Kata Sandi</button>
          </div>
        </form>
    </div>

    <script>
        
    </script>
</div>
@endsection