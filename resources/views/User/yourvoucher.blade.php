@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-bottom: 11%">
    <div class="y-title">
        <h3>Kupon anda</h3>
    </div>
    <div class="container">
        <div class="row">
            @if(sizeof($vouchers) == null)
            <div class="container" style="margin-bottom: 50px;">
                <h4>Anda tidak memiliki kupon</h4><br>
            </div>
            <span><a href="{{url('/reedemvoucher')}}" class="btn btn-submit btn-lg" style="float: right;">Tukar Poin</a></span>

            @else
            <table class="table dataTabel">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Kupon</th>
                        <th>Detail Kupon</th>
                        <th>Batas Waktu</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0; ?>
                    @foreach($vouchers as $voucher)
                    <tr>
                        <td><?php $no = $no + 1; echo $no; ?></td>
                        <td>{{$voucher->voucher->voucher_name}}</td>
                        <td>{{$voucher->voucher->voucher_detail}}</td>
                        <td>{{\Carbon\Carbon::parse($voucher->voucher->expired_date)->format('d-m-Y')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <span><a href="{{url('/reedemvoucher')}}" class="btn btn-submit btn-lg" style="float: right; margin-top: 50px;">Tukar Poin</a></span>
            @endif
        </div>
    </div>
</div>
@endsection