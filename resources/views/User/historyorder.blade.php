@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-bottom: 11%">
    <div class="y-title">
        <h3>Riwayat pesanan anda</h3>
    </div>
    <div class="container">
        <div class="row">
            @if(sizeof($orders) == null)
            <div class="container" style="margin-bottom: 50px;">
                <h4>Anda tidak memiliki kupon</h4><br>
            </div>
            <span><a href="{{url('/productdiameter22')}}" class="btn btn-submit btn-lg" style="float: right;">Belanja</a></span>

            @else
            <table class="table dataTabel">
                <thead>
                    <tr style="border-top: solid 1px #ff4a83;">
                        <th>No.</th>
                        <th>Kode pemesanan</th>
                        <th>Tanggal pengambilan</th>
                        <th>Total Harga</th>
                        <th>Status pemesanan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0; ?>
                    @foreach($orders as $order)
                    <tr>
                        <td><?php $no = $no + 1; echo $no; ?></td>
                        <td><a href="{{url('/historyorderdetail/'.Crypt::encrypt($order->transaction_header_id))}}" style="color: #ff4a83; text-decoration: none;">{{$order->transaction_code}}</a></td>
                        <td>{{\Carbon\Carbon::parse($order->pick_date)->format('d-m-Y')}}</td>
                        <td>Rp. <?php echo number_format($order->total_price, 0, ",", "."); ?></td>

                        @if($order->transaction_status == 0)
                        <td><p class="text-danger">Belum melakukan konfirmasi pembayaran</p><a href="{{url('/paymentconfirmation')}}" style="text-decoration: none;">Lakukan konfirmasi</a></td>
                        @elseif($order->transaction_status == 1)
                        <td><p class="text-success">Sudah Bayar</p></td>
                        @elseif($order->transaction_status == 2)
                        <td><p style="color: #ff4a83;">Proses</p></td>
                        @elseif($order->transaction_status == 3)
                        <td><p class="text-success">Sudah diambil</p></td>
                        @else
                        <td><p class="text-danger">Dibatalkan</p></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
</div>
@endsection