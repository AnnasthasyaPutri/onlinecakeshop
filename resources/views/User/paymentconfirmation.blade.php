@extends('layout.layoutuser')
@section('content')

<div class="container-fluid">
    @if (session('status'))
        <div class="alert alert-success">
          Konfirmasi pembayaran sudah diterima. 
        </div>
    @endif

    <form class="form-horizontal" action="{{url('/paymentconfirmation')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Konfirmasi Pembayaran</h3>
        </div>

        <div class="container">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kode Pemesanan</label>
                <input type="text" name="kode_pemesanan" class="form-control col-md-6 col-sm-6" value="{{old('kode_pemesanan')}}" required>
                @if($errors->first('kode_pemesanan'))<br><div class="alert alert-danger">{{$errors->first('kode_pemesanan')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama Pembayar</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                @if($errors->first('name'))<br><div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Jumlah</label>
                <input type="text" name="jumlah" class="form-control col-md-6 col-sm-6" value="{{old('jumlah')}}" required>
                @if($errors->first('jumlah'))<br><div class="alert alert-danger">{{$errors->first('jumlah')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Bank Asal</label>
                <input type="text" name="bank" class="form-control col-md-6 col-sm-6" value="{{old('bank')}}" required>
                @if($errors->first('bank'))<br><div class="alert alert-danger">{{$errors->first('bank')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Tanggal</label>
                <input type="date" name="tanggal" class="form-control col-md-6 col-sm-6" value="{{old('tanggal')}}" required>
                @if($errors->first('tanggal'))<br><div class="alert alert-danger">{{$errors->first('tanggal')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Bukti Pembayaran</label>
                <input type="file" name="image" value="{{old('image')}}" required>
                @if($errors->first('bukti'))<br><div class="alert alert-danger">{{$errors->first('bukti')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class">
            <a href="{{url('/productdiameter22')}}" class="btn btn-danger">Batal</a>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Konfirmasi">
        </div>
    </form>
</div>
@endsection