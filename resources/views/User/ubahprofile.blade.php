@extends('layout.layoutuser')
@section('content')
<div class="container-fluid row">
    <div class="col-lg-6 col-md-6 col-sm-12 login-form">
        <form class="form-horizontal" action="{{url('/ubahprofile')}}" method="post">
            {{csrf_field()}}
            <div class="y-title">
                <h3>Ubah Profile</h3>
            </div>

            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{$user->user_name}}" required>
                @if($errors->first('name'))<div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>

            <div class="input-group">
                <label class="col-md-3 col-sm-6">Email</label>
                <input type="email" name="email" class="form-control col-md-6 col-sm-6" value="{{$user->user_email}}" required>
                @if($errors->first('email'))<div class="alert alert-danger">{{$errors->first('email')}}</div>@endif
            </div>

            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nomor HP</label>
                <input type="text" name="phone" class="form-control col-md-6 col-sm-6" value="{{$user->user_phone}}" required>
                @if($errors->first('phone'))<div class="alert alert-danger">{{$errors->first('phone')}}</div>@endif
            </div>

            <div class="form-group button-class">
                <input type="submit" class="btn btn-md btn-submit" value="Ubah">
            </div>
        </form>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12 regist-form">
        <form class="form-horizontal" action="{{url('/ubahpassword')}}" method="post">
            {{csrf_field()}}
            <div class="y-title">
                <h3>Ubah Sandi</h3>
            </div>

            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kata Sandi Baru</label>
                <input type="password" name="password" class="form-control col-md-6 col-sm-6" value="{{old('password')}}" required>
                @if($errors->first('password'))<div class="alert alert-danger">{{$errors->first('password')}}</div>@endif
            </div>

            <div class="input-group">
                <label class="col-md-3 col-sm-6">Konfirmasi Kata Sandi</label>
                <input type="password" name="password_confirmation" class="form-control col-md-6 col-sm-6" value="{{old('password_confirmation')}}" required>
                @if($errors->first('password_confirmation'))<div class="alert alert-danger">{{$errors->first('password_confirmation')}}</div>@endif
            </div>

            <div class="form-group button-class">
                <input type="submit" class="btn btn-md btn-submit" value="Ubah">
            </div>
        </form>
    </div>
</div>
@endsection