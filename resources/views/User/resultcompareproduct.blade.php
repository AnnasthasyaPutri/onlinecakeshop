@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12" style="border-right: 1px solid #ff4a83;">
            <form class="form-horizontal" action="/store" method="post">
                {{csrf_field()}}
                <img class="img-thumbnail" src="{{URL::asset('/product/'.$image1)}}" style="margin-left: 20%; height: 300px; width: 300px; border: 2px solid #ff4a83;">
                <div class="detail" style="margin-left: 10%; margin-top: 2%;">
                    <input type="hidden" name="product_id" value="{{$product1->product_id}}">
                    <input type="hidden" name="base" value="{{$product1->base_cake_id}}">
                    <input type="hidden" name="cover" value="{{$product1->cover_cake_id}}">
                    <input type="hidden" name="decor" value="{{$product1->dekorasi_cake_id}}">
                    <input type="hidden" name="calorie" value="{{$product1->calorie}}">
                    <input type="hidden" name="price" value="{{$product1->price}}">
                    <h4>Kue Dasar : {{$product1->basecake->base_cake_name}}</h4>
                    <h4>Lapisan Kue : {{$product1->covercake->cover_cake_name}}</h4>
                    <h4>Dekorasi Kue : {{$product1->dekorasicake->dekorasi_cake_name}}</h4>
                    @if($product1->size_cake == "d22")
                    <h4>Ukuran Kue : Diameter 22</h4>
                    @else
                    <h4>Ukuran Kue : Diameter 18</h4>
                    @endif
                    <h4>Kalori : {{$product1->calorie}} kkal/potong</h4>
                    <h4>Harga : Rp. <?php echo number_format($product1->price, 0, ",", "."); ?></h4><br>
                    <button type="submit" class="btn btn-submit">Beli</button>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <form class="form-horizontal" action="/store" method="post">
                {{csrf_field()}}
                <img class="img-thumbnail" src="{{URL::asset('/product/'.$image2)}}" style="margin-left: 20%; height: 300px; width: 300px; border: 2px solid #ff4a83;">
                <div class="detail" style="margin-left: 10%; margin-top: 2%;">
                    <input type="hidden" name="product_id" value="{{$product2->product_id}}">
                    <input type="hidden" name="base" value="{{$product2->base_cake_id}}">
                    <input type="hidden" name="cover" value="{{$product2->cover_cake_id}}">
                    <input type="hidden" name="decor" value="{{$product2->dekorasi_cake_id}}">
                    <input type="hidden" name="calorie" value="{{$product2->calorie}}">
                    <input type="hidden" name="price" value="{{$product2->price}}">
                    <h4>Kue Dasar : {{$product2->basecake->base_cake_name}}</h4>
                    <h4>Lapisan Kue : {{$product2->covercake->cover_cake_name}}</h4>
                    <h4>Dekorasi Kue : {{$product2->dekorasicake->dekorasi_cake_name}}</h4>
                    @if($product2->size_cake == "d22")
                    <h4>Ukuran Kue : Diameter 22</h4>
                    @else
                    <h4>Ukuran Kue : Diameter 18</h4>
                    @endif
                    <h4>Kalori : {{$product2->calorie}} kkal/potong</h4>
                    <h4>Harga : Rp. <?php echo number_format($product2->price, 0, ",", "."); ?></h4><br>
                    <button type="submit" class="btn btn-submit">Beli</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection