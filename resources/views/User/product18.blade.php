@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <div class="row">
        @foreach($products as $product)
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="img-product">
                <a href="{{url('/viewdetailproduct/'.Crypt::encrypt($product->product_id))}}">
                    <img class="img-thumbnail" src="product/{{$product->product_image}}" style="height: 300px; border-bottom: 3px solid #ff4a83;"><br>
                </a>
                <h3>{{$product->product_name}}</h3>
                <h4>Rp <?php echo number_format($product->price, 0, ",", "."); ?></h4>
                <h4>Kalori : {{$product->calorie}} kkal/potong</h4>
            </div>
        </div>
        @endforeach
    </div>
    <div class="filter">
        <a class="btn-filter" onclick="document.getElementById('filter18').style.display='block'"><i class="fas fa-filter"></i></a>
    </div>

    <div id="filter18" class="modal">
        <form class="modal-content" action="{{url('/filterdiameter18')}}" method="post" style="width: 500px; margin-top: 200px; margin-left: 2%;">
          {{csrf_field()}}
          <div class="container" style="margin-top: 20px;">
            <div class="form-group">
                <label for="price" class="col-lg-2 col-md-2 col-sm-2"><b>Harga</b></label>
                <input type="number" class="col-lg-3 col-md-3 col-sm-3" name="price_min" value="350000" required>
                <label class="col-lg-2 col-md-2 col-sm-2"><b>sampai</b></label>
                <input type="number" class="col-lg-3 col-md-3 col-sm-3" name="price_max" value="700000" required>
            </div>
            <div class="form-group">
                <label for="calorie" class="col-lg-2 col-md-2 col-sm-2"><b>Kalori</b></label>
                <input type="number" class="col-lg-3 col-md-3 col-sm-3" name="cal_min" value="300" required>
                <label class="col-lg-2 col-md-2 col-sm-2"><b>sampai</b></label>
                <input type="number" class="col-lg-3 col-md-3 col-sm-3" name="cal_max" value="1000" required>
            </div>
          </div>
          <div class="btn-modal">
            <button type="button" class="btn btn-danger" onclick="document.getElementById('filter18').style.display='none'">Batal</button>
            <button type="submit" class="btn btn-md btn-submit">Terapkan</button>
          </div>
        </form>
    </div>
</div>
@endsection