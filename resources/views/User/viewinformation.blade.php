@extends('layout.layoutuser')
@section('content')
<div class="container-fluid">
    <div class="info">
        <div class="y-title">
            <h3>Kontak Informasi</h3>
        </div>
        <div class="container">
            <span><i class="fas fa-store"></i></span> <span>{{$info->alamat}}</span><br>
            <span><i class="fas fa-phone"></i></span> <span>{{$info->contact_information}}</span><br>
            <span><i class="fas fa-envelope"></i></span> <span>{{$info->email}}</span><br>
            <span><i class="fab fa-instagram"></i></span> <span>{{$info->instagram}}</span><br>
            <span><i class="fab fa-facebook"></i></span> <span>{{$info->facebook}}</span><br>
            <span><i class="fab fa-twitter"></i></span> <span>{{$info->twitter}}</span><br>
        </div>
    </div>

    <div class="info" style="margin-top: 20px;">
        <div class="y-title">
            <h3>Cara Pemesanan</h3>
        </div>
        <div class="container">
            <span>1. </span> <span>Pilih <b>Design Cake</b> yang disukai.</span><br>
            <span>2. </span> <span>Tentukan akan melakukan <b>Modifikasi Cake</b> atau tidak.</span><br>
            <span>3. </span> <span>Tentukan <b>Base Cake</b> yang diinginkan.</span><br>
            <span>4. </span> <span>Jika melakukan modifikasi, tentukan <b>Lapisan Cake</b> dan <b>Dekorasi Cake</b> yang akan digunakan.</span><br>
            <span>5. </span> <span>Lanjutkan ke <b>Check Out</b>.</span><br>
            <span>6. </span> <span>Pilih <b>Tanggal</b> dan <b>Waktu</b> pengambilan kue di toko kami.</span><br>
            <span>7. </span> <span><b>Jangan Lupa</b> cantumkan <b>Tulisan</b> yang diinginkan pada note.</span><br>
            <span>8. </span> <span>Isi <b>Data Diri</b> yang lengkap untuk kemudahan pemberian notifikasi.</span><br>
            <span>9. </span> <span>Cake yang dibeli sudah termasuk <b>Lilin 5pcs</b>.</span><br>
            <span>10. </span> <span>Pastikan pesanan sudah benar dan lanjutkan untuk melakukan <b>Pemesanan</b>.</span><br>
            <span>11. </span> <span><b>Ingat!</b> cake pesanan akan diproses ketika sudah melakukan <b>Confirmation Payment</b>.</span><br>
            <span>12. </span> <span>Payment dapat melalui <b>Bank Transfer BCA: 123 456 7890</b> a.n <b>Sweet Cake</b></span><br>
            <span>13. </span> <span>Selesai deh! Tinggal tunggu notifikasi dari kami jika ada masalah dan pesanan sudah jadi.</span><br>
        </div>
    </div>
</div>

@endsection