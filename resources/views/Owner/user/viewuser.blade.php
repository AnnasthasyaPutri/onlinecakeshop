@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-top: 50px;">

  @if (session('kirim'))
  <div class="alert alert-success">
      Berhasil kirim kupon ke member!
  </div>
  @endif

  @if(sizeof($users) == 0)
  <h1>Daftar Member</h1>
  <hr>
  <div class="container" style="margin-bottom: 150px;">
      <h3 style="margin-bottom: 50px;">Belum Ada Member!</h3><br>
  </div>
    @else
  <h1>Daftar Member</h1>

    <div class="container" style="margin-bottom: 130px; margin-top: 20px;">
        <div class="row" style="margin-top: 40px; margin-left: 30px; margin-right: 30px;">
          <table class="table dataTable">
            <thead>
              <tr>
                <th style="color: #ff4a83;">Nama</th>
                <th style="color: #ff4a83;">Email</th>
                <th style="color: #ff4a83;">Nomor HP</th>
                <th style="color: #ff4a83;">Jumlah Transaksi</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
              <tr>
                <td><h5>{{$user->user_name}}</h5></td>
                <td>{{$user->user_email}}</td>
                <td>{{$user->user_phone}}</td>
                <td>{{Count($user->transaction)}}</td>
                <td>
                    <button type="button" data-url="{{url('/sendvoucher/'.Crypt::encrypt($user->user_id))}}" class="btn btn-submit btn-send" id="{{$user->user_id}}">Kirim Voucher</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
  @endif
</div>

<div id="kirim" class="modal">
  <form class="modal-content animate modal-kirim" method="POST" style="width: 500px; margin-top: 10%; margin-left: 33%;">
    {{csrf_field()}}
    <div class="imgcontainer">
      <span data-dismiss="modal" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
    </div>

    <br><br>
    <div class="container">
        <p class="col-md-3 col-sm-3">Kupon</p>
        <select class="col-md-9 col-sm-9" required name="voucher" >
            <option value="">Pilih</option>
            @foreach($vouchers as $voucher)
            <option value="{{$voucher->voucher_id}}">{{$voucher->voucher_name}}, {{$voucher->voucher_detail}}</option>
            @endforeach
        </select>
    </div>

    <br><br>
    <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
      <button type="button"  data-dismiss="modal" class="btn btn-danger btn-lg">Batal</button>
      <input type="submit" class="btn btn-primary btn-lg" value="Setuju">
    </div>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
      $('.dataTable').on("click", ".btn-send", function(){
          $('.modal-kirim').attr('action', '');

          var url = $(this).attr('data-url');
          $('.modal-kirim').attr('action', url);
          $('#kirim').modal('show');

      });
  });
</script>
@endsection