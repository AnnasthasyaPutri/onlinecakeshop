@extends('layout.layoutuser')
@section('content')

@if (session('fail'))
  <div class="alert alert-danger">
      Ukuran kue dasar dengan lapisan kue tidak sama!
  </div>
  @endif

<div class="container-fluid">
    <form class="form-horizontal" action="{{url('/addproduct')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Formulir Tambah Produk</h3>
        </div>

        <div class="container">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama Produk</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                @if($errors->first('name'))<br><div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kue Dasar</label>
                <select class="form-control col-md-6 col-sm-6" required name="base" >
                    <option value="">Pilih</option>
                    @foreach($base as $bc)
                    <option value="{{$bc->base_cake_id}}">{{$bc->base_cake_name}} {{$bc->size_cake}}</option>
                    @endforeach
                </select>
                @if($errors->first('base'))<br><div class="alert alert-danger">{{$errors->first('base')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Lapisan Kue</label>
                <select class="form-control col-md-6 col-sm-6" required name="cover" >
                    <option value="">Pilih</option>
                    @foreach($cover as $cc)
                    <option value="{{$cc->cover_cake_id}}">{{$cc->cover_cake_name}} {{$cc->size_cake}}</option>
                    @endforeach
                </select>
                @if($errors->first('cover'))<br><div class="alert alert-danger">{{$errors->first('cover')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Dekorasi Kue</label>
                <select class="form-control col-md-6 col-sm-6" required name="dekor" >
                    <option value="">Pilih</option>
                    @foreach($dekor as $dc)
                    <option value="{{$dc->dekorasi_cake_id}}">{{$dc->dekorasi_cake_name}}</option>
                    @endforeach
                </select>
                @if($errors->first('dekor'))<br><div class="alert alert-danger">{{$errors->first('dekor')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Gambar Kue</label>
                <input type="file" name="image" value="{{old('image')}}">
                @if($errors->first('image'))<br><div class="alert alert-danger">{{$errors->first('image')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class" style="padding-left: 72%;">
            <a href="{{url('/listproduct')}}" class="btn btn-danger">Batal</a>&nbsp;
            <button class="btn btn-primary" type="reset">Reset</button>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Buat">
        </div>
    </form>
</div>
@endsection