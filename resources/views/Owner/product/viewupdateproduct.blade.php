@extends('layout.layoutuser')
@section('content')

<div class="container-fluid">
    <form class="form-horizontal" action="{{url('/updateproduct')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Formulir Ubah Produk</h3>
        </div>

        <div class="container">
            <input type="hidden" name="productId" value="{{$product->product_id}}">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama Produk</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{$product->product_name}}" required>
                @if($errors->first('name'))<br><div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kue Dasar</label>
                <select class="form-control col-md-6 col-sm-6" required name="base" >
                    <option value="{{$product->base_cake_id}}">{{$product->basecake->base_cake_name}}</option>

                    @foreach($base as $bc)
                    <option value="{{$bc->base_cake_id}}">{{$bc->base_cake_name}}</option>
                    @endforeach
                </select>
                @if($errors->first('base'))<br><div class="alert alert-danger">{{$errors->first('base')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Lapisan Kue</label>
                <select class="form-control col-md-6 col-sm-6" required name="cover" >
                    <option value="{{$product->cover_cake_id}}">{{$product->covercake->cover_cake_name}}</option>

                    @foreach($cover as $cc)
                    <option value="{{$cc->cover_cake_id}}">{{$cc->cover_cake_name}}</option>
                    @endforeach
                </select>
                @if($errors->first('cover'))<br><div class="alert alert-danger">{{$errors->first('cover')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Dekorasi Kue</label>
                <select class="form-control col-md-6 col-sm-6" required name="dekor" >
                    <option value="{{$product->dekorasi_cake_id}}">{{$product->dekorasicake->dekorasi_cake_name}}</option>

                    @foreach($dekor as $dc)
                    <option value="{{$dc->dekorasi_cake_id}}">{{$dc->dekorasi_cake_name}}</option>
                    @endforeach
                </select>
                @if($errors->first('dekor'))<br><div class="alert alert-danger">{{$errors->first('dekor')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Gambar Kue</label>
                <input type="file" name="image">
                @if($errors->first('image'))<br><div class="alert alert-danger">{{$errors->first('image')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class" style="padding-left: 72%;">
            <a href="{{url('/listproduct')}}" class="btn btn-danger">Batal</a>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Ubah">
        </div>
    </form>
</div>
@endsection