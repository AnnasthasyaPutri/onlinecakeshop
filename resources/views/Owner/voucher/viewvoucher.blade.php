@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-top: 50px;">

   @if (session('statusok'))
  <div class="alert alert-success">
      Kupon berhasil di tambah!
  </div>
  @endif

  @if (session('statusupdate'))
  <div class="alert alert-success">
      Kupon berhasil di ubah!
  </div>
  @endif

  @if (session('statushapus'))
  <div class="alert alert-success">
      Kupon berhasil di hapus!
  </div>
  @endif

  @if(sizeof($vouchers) == 0)
  <h1>Daftar Kupon</h1>
  <hr>
  <div class="container" style="margin-bottom: 50px;">
      <h3 style="margin-bottom: 50px;">Belum Ada Kupon. Ayo buat Kupon!</h3><br>
  </div>
  <span><a href="{{url('/viewaddvoucherform')}}" class="btn btn-submit btn-lg" style="margin-left: 50px;">Tambah</a></span>
    @else
  <h1>Daftar Kupon <span style="margin-left: 2%;"><a href="{{url('/viewaddvoucherform')}}" class="btn btn-submit btn-lg">Tambah</a></span></h1>

    <div style="margin-bottom: 130px; margin-top: 20px;">
        <div class="row" style="margin-top: 40px; margin-left: 30px; margin-right: 30px;">
          <table class="table dataTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Kupon</th>
                    <th>Awal Berlaku</th>
                    <th>Akhir Berlaku</th>
                    <th>Detail Kupon</th>
                    <th>Quantity</th>
                    <th>Point</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0; ?>
                @foreach($vouchers as $voucher)
                <tr>
                    <td><?php $no = $no + 1; echo $no; ?></td>
                    <td>{{$voucher->voucher_name}}</td>
                    <td>{{\Carbon\Carbon::parse($voucher->start_date)->format('d-m-Y')}}</td>
                    <td>{{\Carbon\Carbon::parse($voucher->expired_date)->format('d-m-Y')}}</td>
                    <td>{{$voucher->voucher_detail}}</td>
                    <td>{{$voucher->qty}}</td>
                    <td>{{$voucher->point}}</td>
                    <td>
                        <a href="{{url('/updatevoucher/'.Crypt::encrypt($voucher->voucher_id))}}" class="btn btn-md btn-primary" style="margin-bottom: 5px;">Ubah</a>&nbsp;
                        <button data-url="{{url('/deletevoucher/'.Crypt::encrypt($voucher->voucher_id))}}" class='btn btn-md btn-danger btn-hapus'>Hapus</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
    </div>
  @endif
</div>

<div id="hapus" class="modal">
  <form class="modal-content animate modal-hapus" method="POST" style="width: 500px; margin-top: 10%; margin-left: 33%;">
    {{csrf_field()}}
    <div class="imgcontainer">
      <span data-dismiss="modal" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
    </div>

    <br><br>
    <div class="container">
        <p style="text-align: center;">Yakin akan hapus kupon ini?</p>
    </div>

    <br><br>
    <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
      <button type="button"  data-dismiss="modal" class="btn btn-danger btn-lg">Batal</button>
      <input type="submit" class="btn btn-primary btn-lg" value="Setuju">
    </div>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
      $('.dataTable').on("click", ".btn-hapus", function(){
          $('.modal-hapus').attr('action', '');

          var url = $(this).attr('data-url');
          $('.modal-hapus').attr('action', url);
          $('#hapus').modal('show');

      });
  });
</script>
@endsection