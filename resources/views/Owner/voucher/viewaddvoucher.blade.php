@extends('layout.layoutuser')
@section('content')

<div class="container-fluid">
    <form class="form-horizontal" action="{{url('/addvoucher')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Formulir Tambah Kupon</h3>
        </div>

        <div class="container">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama kupon</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                @if($errors->first('name'))<br><div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Tipe kupon</label>
                <select class="form-control col-md-6 col-sm-6" required name="tipe" id="tipe">
                    <option value="">Pilih</option>
                    <option value="1">Pembelian Kue Dasar</option>
                    <option value="2">Pembelian Lapisan Kue</option>
                    <option value="3">Pembelian Dekorasi Kue</option>
                    <option value="4">Pembelian Product</option>
                    <option value="5">Minimum Harga</option>
                </select>
                @if($errors->first('tipe'))<br><div class="alert alert-danger">{{$errors->first('tipe')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Detail Kupon</label>
                <textarea name="detail" class="form-control col-md-6 col-sm-6" required>{{old('detail')}}</textarea> 
                @if($errors->first('detail'))<br><div class="alert alert-danger">{{$errors->first('detail')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Harga Poin</label>
                <input type="number" name="poin" class="form-control col-md-6 col-sm-6" value="{{old('poin')}}" required>
                @if($errors->first('poin'))<br><div class="alert alert-danger">{{$errors->first('poin')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Quantity</label>
                <input type="number" name="qty" class="form-control col-md-6 col-sm-6" value="{{old('qty')}}" required>
                @if($errors->first('qty'))<br><div class="alert alert-danger">{{$errors->first('qty')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Jumlah Potongan</label>
                <input type="number" name="discount" class="form-control col-md-6 col-sm-6" value="{{old('discount')}}">
                @if($errors->first('discount'))<br><div class="alert alert-danger">{{$errors->first('discount')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Persen Potongan</label>
                <input type="number" name="persen" class="form-control col-md-6 col-sm-6" value="{{old('persen')}}">
                @if($errors->first('persen'))<br><div class="alert alert-danger">{{$errors->first('persen')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Minimum Pembelian / Harga</label>
                <input type="number" name="minimum" class="form-control col-md-6 col-sm-6" value="{{old('minimum')}}" required>
                @if($errors->first('minimum'))<br><div class="alert alert-danger">{{$errors->first('minimum')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Tanggal Mulai Berlaku</label>
                <input type="date" name="start" class="form-control col-md-6 col-sm-6" value="{{old('start')}}" required>
                @if($errors->first('start'))<br><div class="alert alert-danger">{{$errors->first('start')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Tanggal Akhir Berlaku</label>
                <input type="date" name="expired" class="form-control col-md-6 col-sm-6" value="{{old('expired')}}" required>
                @if($errors->first('expired'))<br><div class="alert alert-danger">{{$errors->first('expired')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kue Dasar</label>
                <select class="form-control col-md-6 col-sm-6" name="base" disabled="true" id="base">
                    <option value="">Pilih</option>
                    @foreach($bases as $base)
                    <option value="{{$base->base_cake_id}}">{{$base->base_cake_name}} {{$base->size_cake}}</option>
                    @endforeach
                </select>
                @if($errors->first('base'))<br><div class="alert alert-danger">{{$errors->first('base')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Lapisan Kue</label>
                <select class="form-control col-md-6 col-sm-6" name="cover" disabled="true" id="cover">
                    <option value="">Pilih</option>
                    @foreach($covers as $cover)
                    <option value="{{$cover->cover_cake_id}}">{{$cover->cover_cake_name}} {{$cover->size_cake}}</option>
                    @endforeach
                </select>
                @if($errors->first('cover'))<br><div class="alert alert-danger">{{$errors->first('cover')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Dekorasi Kue</label>
                <select class="form-control col-md-6 col-sm-6" name="decor" disabled="true" id="decor">
                    <option value="">Pilih</option>
                    @foreach($decors as $decor)
                    <option value="{{$decor->dekorasi_cake_id}}">{{$decor->dekorasi_cake_name}}</option>
                    @endforeach
                </select>
                @if($errors->first('decor'))<br><div class="alert alert-danger">{{$errors->first('decor')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Product</label>
                <select class="form-control col-md-6 col-sm-6" name="product" disabled="true" id="product">
                    <option value="">Pilih</option>
                    @foreach($products as $product)
                    <option value="{{$product->product_id}}">{{$product->product_name}} {{$product->size_cake}}</option>
                    @endforeach
                </select>
                @if($errors->first('product'))<br><div class="alert alert-danger">{{$errors->first('product')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class" style="padding-left: 72%;">
            <a href="{{url('/listvoucher')}}" class="btn btn-danger">Batal</a>&nbsp;
            <button class="btn btn-primary" type="reset">Reset</button>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Buat">
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tipe').change(function(){
            var tipe = $('#tipe').val();
            if(tipe == 1){
                $('#base').attr('disabled',false);
                $('#cover').attr('disabled',true);
                $('#decor').attr('disabled',true);
                $('#product').attr('disabled',true);
            }
            else if(tipe == 2){
                $('#cover').attr('disabled',false);
                $('#base').attr('disabled',true);
                $('#decor').attr('disabled',true);
                $('#product').attr('disabled',true);
            }
            else if(tipe == 3){
                $('#decor').attr('disabled',false);
                $('#base').attr('disabled',true);
                $('#cover').attr('disabled',true);
                $('#product').attr('disabled',true);
            }
            else if(tipe == 4){
                $('#product').attr('disabled',false);
                $('#base').attr('disabled',true);
                $('#cover').attr('disabled',true);
                $('#decor').attr('disabled',true);
            }
            else{
                $('#base').attr('disabled',true);
                $('#cover').attr('disabled',true);
                $('#decor').attr('disabled',true);
                $('#product').attr('disabled',true);
            }
        });
    });
</script>
@endsection