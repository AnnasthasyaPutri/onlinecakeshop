@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-bottom: 13%; margin-left: 2%;">
    <div class="y-title">
        <h3>Kontak Informasi</h3>
    </div>
    <div class="container">
        <span><i class="fas fa-store"></i> {{$info->alamat}}</span><br>
        <span><i class="fas fa-user"></i> {{$user->user_name}}</span><br>
        <span><i class="fas fa-phone"></i> {{$info->contact_information}}</span><br>
        <span><i class="fas fa-envelope"></i> {{$info->email}}</span><br>
        <span><i class="fab fa-instagram"></i></span> <span>{{$info->instagram}}</span><br>
        <span><i class="fab fa-facebook"></i></span> <span>{{$info->facebook}}</span><br>
        <span><i class="fab fa-twitter"></i></span> <span>{{$info->twitter}}</span><br><br>

        <div class="btn-class"><a href="{{url('/ubahinfo')}}" class="btn btn-md btn-submit">Ubah</a></div>
    </div>
</div>
@endsection