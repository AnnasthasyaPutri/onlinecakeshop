@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-top: 50px;">

  @if (session('statusok'))
  <div class="alert alert-success">
      Lapisan kue berhasil di tambah!
  </div>
  @endif

  @if (session('statusupdate'))
  <div class="alert alert-success">
      Lapisan kue berhasil di ubah!
  </div>
  @endif

  @if (session('statushapus'))
  <div class="alert alert-success">
      Lapisan kue berhasil di hapus!
  </div>
  @endif

  @if(sizeof($covers) == 0)
  <h1>Daftar Lapisan Kue</h1>
  <hr>
  <div class="container" style="margin-bottom: 50px;">
      <h3 style="margin-bottom: 50px;">Belum Ada Lapisan Kue. Ayo buat lapisan kue!</h3><br>
  </div>
  <span><a href="{{url('/viewaddcoverform')}}" class="btn btn-submit btn-lg" style="margin-left: 50px;">Tambah</a></span>
	@else
  <h1>Daftar Lapisan Kue <span style="margin-left: 2%;"><a href="{{url('/viewaddcoverform')}}" class="btn btn-submit btn-lg">Tambah</a></span></h1>

    <div class="row" style="margin-top: 20px; margin-left: 10px; margin-right: 10px;">
      <table class="table dataTable">
        <thead>
          <tr>
            <th style="color: #ff4a83;">Gambar</th>
            <th style="color: #ff4a83;">Nama</th>
            <th style="color: #ff4a83;">Ukuran Kue</th>
            <th style="color: #ff4a83;">Harga Kue</th>
            <th style="color: #ff4a83;">Kalori Kue</th>
            <th style="color: #ff4a83;">Pembeli</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach($covers as $cover)
          <tr>
            <td><img src="cover/{{$cover->cover_image}}" alt="item" style="width: 100px; height: 100px;"></td>
            <td style="color: #ff4a83;"><h5>{{$cover->cover_cake_name}}</h5></td>
            @if($cover->size_cake == "d22")
            <td>Diameter 22</td>
            @else
            <td>Diameter 18</td>
            @endif
            <td>Rp. <?php echo number_format($cover->price, 0, ",", "."); ?></td>
            <td>{{$cover->calorie}} kkal/potong</td>
            <td>{{$cover->flag_populer}}</td>
            <td>
              <a href="{{url('/viewupdatecoverform/'.Crypt::encrypt($cover->cover_cake_id))}}" class="btn btn-md btn-primary" style="margin-bottom: 2px;">Ubah</a>
              <button type="button" data-url="{{url('/deletecovercake/'.Crypt::encrypt($cover->cover_cake_id))}}" class="btn btn-danger btn-hapus" id="{{$cover->cover_cake_id}}">Hapus</button>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  @endif
</div>

<div id="hapus" class="modal">
  <form class="modal-content animate modal-hapus" method="POST" style="width: 420px; margin-top: 10%; margin-left: 33%;">
    {{csrf_field()}}
    <div class="imgcontainer">
      <span data-dismiss="modal" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
    </div>

    <br><br>
    <div class="container">
      <p style="text-align: center;">Yakin akan hapus lapisan kue ini?</p>
    </div>

    <br><br>
    <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
      <button type="button"  data-dismiss="modal" class="btn btn-danger btn-lg">Batal</button>
      <input type="submit" class="btn btn-primary btn-lg" value="Setuju">
    </div>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
      $('.dataTable').on("click", ".btn-hapus", function(){
          $('.modal-hapus').attr('action', '');

          var url = $(this).attr('data-url');
          $('.modal-hapus').attr('action', url);
          $('#hapus').modal('show');

      });
  });
</script>
@endsection