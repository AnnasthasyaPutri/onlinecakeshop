@extends('layout.layoutuser')
@section('content')

<div class="container-fluid">
    <form class="form-horizontal" action="{{url('/addcovercake')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Formulir Tambah Lapisan Kue</h3>
        </div>

        <div class="container">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama Lapisan</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{old('name')}}" required>
                @if($errors->first('name'))<br><div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Ukuran Lapisan</label>
                <select class="form-control col-md-6 col-sm-6" required name="size" >
                    <option value="">Pilih</option>
                    <option value="d18">Diameter 18</option>
                    <option value="d22">Diameter 22</option>
                </select>
                @if($errors->first('size'))<br><div class="alert alert-danger">{{$errors->first('size')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Harga</label>
                <input type="number" name="price" class="form-control col-md-6 col-sm-6" value="{{old('price')}}" required>
                @if($errors->first('price'))<br><div class="alert alert-danger">{{$errors->first('price')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kalori</label>
                <input type="number" name="calorie" class="form-control col-md-6 col-sm-6" value="{{old('calorie')}}" required>
                @if($errors->first('calorie'))<br><div class="alert alert-danger">{{$errors->first('calorie')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Gambar Kue</label>
                <input type="file" name="image" value="{{old('image')}}">
                @if($errors->first('image'))<br><div class="alert alert-danger">{{$errors->first('image')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class" style="padding-left: 72%;">
            <a href="{{url('/listcovercake')}}" class="btn btn-danger">Batal</a>&nbsp;
            <button class="btn btn-primary" type="reset">Reset</button>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Buat">
        </div>
    </form>
</div>
@endsection