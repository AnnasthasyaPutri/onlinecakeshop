@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-bottom: 11%">
    <div class="y-title">
        <h3>Detail Pesanan</h3>
    </div>
    <div class="container">
        <div class="row">
            <table class="table dataTabel">
                <tbody>
                    <?php $no = 0; ?>
                    @foreach($orders as $order)
                    <tr style="border-top: solid 1px #ff4a83;">
                        <td><img src="../{{$order->pic}}" alt="item" class="img" style="width: 100px; height: 100px;"></td>
                        <td><h5 style="color: #ff4a83;">{{$order->product_name}}</h5><p>{{$order->basecake->base_cake_name}}, {{$order->covercake->cover_cake_name}}, {{$order->dekorasicake->dekorasi_cake_name}}, {{$order->size_cake}}, {{$order->calorie}} kkal/potong</p></td>
                        <td>Note: {{$order->note}}</td>
                        <td>Qty: {{$order->qty}}</td>
                        <td>Rp. <?php echo number_format($order->price, 0, ",", "."); ?></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <span><a href="{{url('/listorder')}}" class="btn btn-submit btn-lg" style="float: right; margin-top: 50px;">Kembali</a></span>
        </div>
    </div>
</div>
@endsection