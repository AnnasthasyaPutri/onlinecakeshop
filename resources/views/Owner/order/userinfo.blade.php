@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-bottom: 14%; margin-left: 3%;">
    <div class="y-title">
        <h3>Data Pembeli</h3>
    </div>
    <div class="container">
            <h5 style="text-align: left;">Nama : {{$trsc->user_name}}</h5><br>
            <h5 style="text-align: left;">Email : {{$trsc->user_email}}</h5><br>
            <h5 style="text-align: left;">Nomor HP : {{$trsc->user_phone}}</h5>            
            <span><a href="{{url('/listorder')}}" class="btn btn-submit btn-lg" style="float: right; margin-top: 50px;">Kembali</a></span>
    </div>
</div>
@endsection