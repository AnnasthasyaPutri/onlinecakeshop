@extends('layout.layoutuser')
@section('content')
<div class="container-fluid" style="margin-top: 50px;">

  @if (session('statuscancel'))
  <div class="alert alert-success">
      Pesanan berhasil di batalkan!
  </div>
  @endif

  @if (session('statuspickup'))
  <div class="alert alert-success">
      Pesanan sudah di ambil!
  </div>
  @endif

  @if (session('statusupdate'))
  <div class="alert alert-success">
      Tanggal Pengambilan berhasil di ubah!
  </div>
  @endif

  @if (session('statuspayment'))
  <div class="alert alert-success">
      Pembayaran sudah disetujui!
  </div>
  @endif

  @if (session('statusingat'))
  <div class="alert alert-success">
      Berhasil kirim email pengingat!
  </div>
  @endif

  @if (session('statusbatalupdate'))
  <div class="alert alert-danger">
      Tanggal lebih kecil dari hari ini!
  </div>
  @endif

  @if(sizeof($orders) == 0)
  <h1>Daftar Pesanan</h1>
  <hr>
  <div class="container" style="margin-bottom: 150px;">
      <h3 style="margin-bottom: 50px;">Belum Ada Pesanan!</h3><br>
  </div>
    @else
  <h1>Daftar Pesanan</h1>

    <div class="container" style="margin-bottom: 130px; margin-top: 20px;">
        <div class="row" style="margin-top: 40px; margin-left: 30px; margin-right: 30px;">
          <table class="table dataTable">
            <thead>
                <tr>
                    <th>Kode Pemesanan</th>
                    <th>Tanggal Pengambilan</th>
                    <th>Total Harga</th>
                    <th>Pemesan</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              <tr>
                <td><a href="{{url('/orderdetail/'.Crypt::encrypt($order->transaction_header_id))}}" style="color: #ff4a83; text-decoration: none;">{{$order->transaction_code}}</a></td>
                <td>{{\Carbon\Carbon::parse($order->pick_date)->format('d-m-Y')}}</td>
                <td>Rp. <?php echo number_format($order->total_price, 0, ",", "."); ?></td>
                <td><a href="{{url('/viewbuyer/'.Crypt::encrypt($order->transaction_header_id))}}" style="cursor: pointer; text-decoration: none;">{{$order->user_name}}</a>
                @if($order->transaction_status == 0)
                <td>
                  <p class="text-danger">Belum konfirmasi</p>
                </td>
                <td>
                  <a href="{{url('/rememberpayment/'.Crypt::encrypt($order->transaction_header_id))}}" class="btn btn-md btn-primary">Ingatkan</a>
                  <button data-url="{{url('/cancelorder/'.Crypt::encrypt($order->transaction_header_id))}}" class='btn btn-md btn-danger btn-cancel' onclick="document.getElementById('cancel').style.display='block'">Batalkan</button>
                  <button data-id="{{$order->transaction_header_id}}" data="{{$order->pick_date}}" class='btn btn-md btn-ubah' onclick="document.getElementById('Ubah').style.display = 'block'" style="background-color: #ff4a83; color: white;">Ubah</button>
                </td>
                @elseif($order->transaction_status == 1)
                <td>
                  <p class="text-success">Sudah dibayar</p>
                </td>
                <td>
                  <a href="{{url('/viewpaymentconfirmation/'.Crypt::encrypt($order->transaction_header_id))}}" class="btn btn-md btn-primary">Setujui Pembayaran</a>
                  <button data-url="{{url('/cancelorder/'.Crypt::encrypt($order->transaction_header_id))}}" class='btn btn-md btn-danger btn-cancel' onclick="document.getElementById('cancel').style.display='block'">Batalkan</button>
                  <button data-id="{{$order->transaction_header_id}}" data="{{$order->pick_date}}" class='btn btn-md btn-ubah' onclick="document.getElementById('Ubah').style.display = 'block'" style="background-color: #ff4a83; color: white;">Ubah</button>
                </td>
                @elseif($order->transaction_status == 2)
                <td>
                  <p style="color: #ff4a83;">Proses</p>
                </td>
                <td>
                  <a href="{{url('/approvedpickup/'.Crypt::encrypt($order->transaction_header_id))}}" class="btn btn-md btn-success">Diambil</a>
                  <button data-url="{{url('/cancelorder/'.Crypt::encrypt($order->transaction_header_id))}}" class='btn btn-md btn-danger btn-cancel' onclick="document.getElementById('cancel').style.display='block'">Batalkan</button>
                  <button data-id="{{$order->transaction_header_id}}" data="{{$order->pick_date}}" class='btn btn-md btn-ubah' onclick="document.getElementById('Ubah').style.display = 'block'" style="background-color: #ff4a83; color: white;">Ubah</button>
                </td>
                @elseif($order->transaction_status == 3)
                <td>
                  <p>Sudah diambil</p>
                </td>
                <td></td>
                @else
                <td>
                  <p>Dibatalkan</p>
                </td>
                <td></td>
                @endif
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
  @endif
</div>

<div id="cancel" class="modal">
  <form class="modal-content animate modal-cancel" method="POST" style="width: 500px; margin-top: 10%; margin-left: 33%;">
    {{csrf_field()}}
    <div class="imgcontainer">
      <span onclick="document.getElementById('cancel').style.display='none'" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
    </div>

    <br><br>
    <div class="container">
        <p style="text-align: center;">Yakin akan batalkan pesanan ini?</p>
    </div>

    <br><br>
    <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
      <button type="button"  onclick="document.getElementById('cancel').style.display='none'" class="btn btn-danger btn-lg">Batal</button>
      <input type="submit" class="btn btn-primary btn-lg" value="Setuju">
    </div>
  </form>
</div>

<div id="Ubah" class="modal">
  <form class="modal-content animate modal-ubah" action="{{url('/updatepickorder')}}" method="POST" style="width: 300px; margin-top: 10%; margin-left: 33%;">
    {{csrf_field()}}
    <div class="imgcontainer">
      <span onclick="document.getElementById('Ubah').style.display = 'none'" class="close" title="Close Modal" style="cursor: pointer;">&times;</span>
    </div>

    <br><br>
    <div class="container">
      <input type="hidden" name="trscid" id="trsc">
      <p>Tanggal Pengambilan</p><input type="date" name="pick_date" id="trsc_date">
    </div>

    <br><br>
    <div class="container" style="background-color:#f1f1f1 ; margin:0 auto !important; text-align: center !important; padding:10px;">
      <button type="button" onclick="document.getElementById('Ubah').style.display = 'none'"  class="btn btn-danger btn-lg">Batal</button>
      <input type="submit" class="btn btn-primary btn-lg" value="Setuju">
    </div>
  </form>
</div>

<script type="text/javascript">
  $(document).ready(function(){
      $('.dataTable').on("click", ".btn-cancel", function(){
        $('.modal-cancel').attr('action', '');
        var url = $(this).attr('data-url');
        $('.modal-cancel').attr('action', url);
      });

      $('.dataTable').on("click", ".btn-ubah", function(){
        var id = $(this).attr('data-id');
        var date = $(this).attr('data');
        $('#trsc').attr('value', id);
        $('#trsc_date').attr('value',date);
      });
  });
</script>
@endsection