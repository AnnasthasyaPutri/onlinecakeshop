@extends('layout.layoutuser')
@section('content')

<div class="container-fluid">
    <form class="form-horizontal" action="{{url('/approvedpayment')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Data Konfirmasi Pembayaran</h3>
        </div>

        <div class="container">
            <input type="hidden" name="id" value="{{$order->transaction_header_id}}">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kode Pemesanan</label>
                <label class="col-md-3 col-sm-6">{{$order->transaction_code}}</label>
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama Pembayar</label>
                <label class="col-md-3 col-sm-6">{{$order->datapembayaran->atas_nama}}</label>
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Jumlah</label>
                <label class="col-md-3 col-sm-6">Rp. <?php echo number_format($order->datapembayaran->jumlah_pembayaran, 0, ",", "."); ?></label>
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Bank Asal</label>
                <label class="col-md-3 col-sm-6">{{$order->datapembayaran->bank_asal}}</label>
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Tanggal</label>
                <label class="col-md-3 col-sm-6">{{\Carbon\Carbon::parse($order->datapembayaran->tanggal_pembayaran)->format('d-m-Y')}}</label>
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Bukti Pembayaran</label>
                <a href="{{url('/downloadpaymentconfirmation/'.Crypt::encrypt($order->transaction_header_id))}}"><button type="button" class='btn btn-primary btn-lg'>Unduh</button></a>
            </div>
        </div>

        <div class="input-group btn-class">
            <a href="{{url('/listorder')}}" class="btn btn-danger">Batal</a>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Setuju">
        </div>
    </form>
</div>
@endsection