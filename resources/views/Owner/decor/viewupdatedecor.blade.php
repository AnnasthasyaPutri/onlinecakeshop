@extends('layout.layoutuser')
@section('content')

<div class="container-fluid">
    <form class="form-horizontal" action="{{url('/updatedecorcake')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="y-title">
            <h3>Formulir Ubah Dekorasi Kue</h3>
        </div>

        <div class="container">
            <input type="hidden" name="decorId" value="{{$decor->dekorasi_cake_id}}">
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Nama Dekorasi</label>
                <input type="text" name="name" class="form-control col-md-6 col-sm-6" value="{{$decor->dekorasi_cake_name}}" required>
                @if($errors->first('name'))<br><div class="alert alert-danger">{{$errors->first('name')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Harga</label>
                <input type="number" name="price" class="form-control col-md-6 col-sm-6" value="{{$decor->price}}" required>
                @if($errors->first('price'))<br><div class="alert alert-danger">{{$errors->first('price')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Kalori</label>
                <input type="number" name="calorie" class="form-control col-md-6 col-sm-6" value="{{$decor->calorie}}" required>
                @if($errors->first('calorie'))<br><div class="alert alert-danger">{{$errors->first('calorie')}}</div>@endif
            </div>
            <div class="input-group">
                <label class="col-md-3 col-sm-6">Gambar Dekorasi Kue</label>
                <input type="file" name="image">
                @if($errors->first('image'))<br><div class="alert alert-danger">{{$errors->first('image')}}</div>@endif
            </div>
        </div>

        <div class="input-group btn-class" style="padding-left: 72%;">
            <a href="{{url('/listdecorcake')}}" class="btn btn-danger">Batal</a>&nbsp;
            <input type="submit" class="btn btn-md btn-submit" value="Ubah">
        </div>
    </form>
</div>
@endsection