<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kustom extends Model
{
    use SoftDeletes;
    protected $table= "mskustom";
    protected $primaryKey = "kustom_id";
}
