<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;
    protected $table= "msuser";
    protected $primaryKey = "user_id";

    public function transaction(){
    	return $this->hasMany('App\Model\HeaderTransaction','user_email','user_email');
    }

    public function notif(){
        return $this->hasMany('App\Model\Notif','user_id','user_id');
    }

    public function voucher(){
        return $this->hasMany('App\Model\VoucherUser','user_id','user_id');
    }
}
