<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailInformation extends Model
{
    use SoftDeletes;
    protected $table= "msinformation";
}
