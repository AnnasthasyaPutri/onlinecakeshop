<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataPembayaran extends Model
{
    protected $table= "msdata_pembayaran";
    protected $primaryKey = "data_pembayaran_id";

    public function headertransaction(){
    	return $this->belongsTo('App\Model\HeaderTransaction','transaction_header_id','transaction_header_id');
    }
}
