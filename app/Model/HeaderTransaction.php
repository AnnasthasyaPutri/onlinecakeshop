<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HeaderTransaction extends Model
{
    use SoftDeletes;
    protected $table= "mstransaction_header";
    protected $primaryKey = "transaction_header_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_email','user_email');
    }
    
    public function detailtransaction(){
    	return $this->hasMany('App\Model\DetailTransaction','transaction_header_id','transaction_header_id');
    }

    public function datapembayaran(){
        return $this->hasOne('App\Model\DataPembayaran','transaction_header_id','transaction_header_id');
    }
}
