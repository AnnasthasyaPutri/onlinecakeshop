<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use SoftDeletes;
    protected $table= "msvoucher";
    protected $primaryKey = "voucher_id";

    public function voucheruser(){
    	return $this->hasMany('App\Model\VoucherUser','voucher_id','voucher_id');
    }
}
