<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notif extends Model
{
    protected $table= "msnotif";
    protected $primaryKey = "notif_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }


}
