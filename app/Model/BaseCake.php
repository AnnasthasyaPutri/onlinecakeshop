<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseCake extends Model
{
    use SoftDeletes;
    protected $table= "msbase_cake";
    protected $primaryKey = "base_cake_id";

    public function transactiondetail(){
    	return $this->hasMany('App\Model\DetailTransaction','base_cake_id','base_cake_id');
    }

    public function product(){
    	return $this->hasMany('App\Model\Product','base_cake_id','base_cake_id');
    }
}
