<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DekorasiCake extends Model
{
    use SoftDeletes;
    protected $table= "msdekorasi_cake";
    protected $primaryKey = "dekorasi_cake_id";

    public function transactiondetail(){
    	return $this->hasMany('App\Model\DetailTransaction','dekorasi_cake_id','dekorasi_cake_id');
    }

    public function product(){
    	return $this->hasMany('App\Model\Product','dekorasi_cake_id','dekorasi_cake_id');
    }
}
