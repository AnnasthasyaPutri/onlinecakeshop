<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoverCake extends Model
{
    use SoftDeletes;
    protected $table= "mscover_cake";
    protected $primaryKey = "cover_cake_id";

    public function transactiondetail(){
    	return $this->hasMany('App\Model\DetailTransaction','cover_cake_id','cover_cake_id');
    }

    public function product(){
    	return $this->hasMany('App\Model\Product','cover_cake_id','cover_cake_id');
    }
}
