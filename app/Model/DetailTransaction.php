<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailTransaction extends Model
{
    use SoftDeletes;
    protected $table= "mstransaction_detail";
    protected $primaryKey = "transaction_detail_id";

    public function headertransaction(){
    	return $this->belongsTo('App\Model\HeaderTransaction','transaction_header_id','transaction_header_id');
    }

    public function basecake(){
        return $this->belongsTo('App\Model\BaseCake','base_cake_id','base_cake_id');
    }

    public function covercake(){
        return $this->belongsTo('App\Model\CoverCake','cover_cake_id','cover_cake_id');
    }

    public function dekorasicake(){
    	return $this->belongsTo('App\Model\DekorasiCake','dekorasi_cake_id','dekorasi_cake_id');
    }

    public function product(){
        return $this->belongsTo('App\Model\Product','product_id','product_id');
    }
}
