<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoucherUser extends Model
{
    use SoftDeletes;
    protected $table= "msvoucher_user";
    protected $primaryKey = "voucher_user_id";

    public function voucher(){
    	return $this->belongsTo('App\Model\Voucher','voucher_id','voucher_id');
    }

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }
}
