<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table= "msproduct";
    protected $primaryKey = "product_id";

    public function basecake(){
        return $this->belongsTo('App\Model\BaseCake','base_cake_id','base_cake_id');
    }

    public function covercake(){
        return $this->belongsTo('App\Model\CoverCake','cover_cake_id','cover_cake_id');
    }

    public function dekorasicake(){
    	return $this->belongsTo('App\Model\DekorasiCake','dekorasi_cake_id','dekorasi_cake_id');
    }

    public function transaction(){
        return $this->hasMany('App\Model\DetailTransaction','product_id','product_id');
    }
}
