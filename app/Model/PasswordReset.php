<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordReset extends Model
{
    protected $table= "password_resets";
    protected $primaryKey = "reset_id";

    public function user(){
    	return $this->belongsTo('App\Model\User','user_id','user_id');
    }


}
