<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ProductCategory;
use App\Model\Product;
use App\Model\Notif;
use Crypt;
use \Cart as Cart;

class HomeController extends Controller
{
  	public function index(){
  		// $ProductCategory	= ProductCategory::where('flag_delete','=','0')->get();
  		// $Products 			= Product::where('flag_delete','=','0')->get();
  		return view('welcome');
  	}
}
