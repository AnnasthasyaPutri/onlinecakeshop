<?php

namespace App\Http\Controllers\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Notif;
use App\Model\Password_Reset;
use App\Model\Product;
use Response;
use Hash;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;
use Auth;
use DateTime;
use \Cart as Cart;

class FilterController extends Controller
{
  	public function filterd22(Request $request){
  		// dd($request);
		$products = Product::where('size_cake','d22')->where('flag_delete',0)->where('price','>=',$request->price_min)->where('price','<=',$request->price_max)->where('calorie','>=',$request->cal_min)->where('calorie','<=',$request->cal_max)->get();
		return view('User.product22',compact('products'));
  	}

  	public function filterd18(Request $request){
  		$products = Product::where('size_cake','d18')->where('flag_delete',0)->where('price','>=',$request->price_min)->where('price','<=',$request->price_max)->where('calorie','>=',$request->cal_min)->where('calorie','<=',$request->cal_max)->get();
  		return view('User.product18',compact('products'));
  	}
}
