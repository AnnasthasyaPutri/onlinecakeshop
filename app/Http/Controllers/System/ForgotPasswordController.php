<?php

namespace App\Http\Controllers\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Notif;
use App\Model\PasswordReset;
use Response;
use Hash;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;
use Auth;
use DateTime;
use \Cart as Cart;

class ForgotPasswordController extends Controller
{
  	public function forgotemail(Request $request){
        if(Auth::check() == true){
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                return redirect('/listproduct');
            }
        }
        else{
            $users = User::where('user_email',$request->user_email)->first();
            if($users != null){
                $error = true;
                $user = User::where('user_email',$request->user_email)->first();
                $token = '';
                $message = '';
                $data = [];

                try{
                    $token = str_random(50);
                    $email_sendto = $request->user_email;
                    $data['path'] = url('/forgot/'.$token);

                    Mail::send('Mail.forgot', $data, function($message) use($email_sendto) {
                        $message->to($email_sendto)->subject('Password Reset');
                    });

                    DB::beginTransaction();

                    DB::table('password_resets')->insert(
                        ['user_id' => $user->user_id, 'email' => $user->user_email, 'token' => $token]
                    );

                    DB::commit();

                    return redirect('/login')->with('status','Success');
                }
                catch(Exception $e){
                    DB::rollBack();
                }
            }
            else{
                return redirect('/login')->with('status1','Danger');
            }
        }
  	}

  	public function forgotview($token){
        if(Auth::check() == true){
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                return redirect('/listproduct');
            }
        }
        else{
            $reset = PasswordReset::where('token',$token)->first();
            if($reset == null){
                return redirect('/login')->with('status1','Danger');
            }
            else{
                $data = $token;
                return view('User.forgotpassword',compact('data'));
            }
        }
	}

	public function changepassword(Request $request){
        if(Auth::check() == true){
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                return redirect('/listproduct');
            }
        }
        else{
            $message = [
                'email.required'            =>'Email Tidak Boleh Kosong',
                'email.email'               =>'Format Email Salah',
                'email.exists'              =>'Email anda tidak terdaftar',
                'password.required'         =>'Password tidak boleh kosong',
                'password.min'              =>'Password minimal 6 karakter',
                'password.confirmed'        =>'Password dan Confirm Password Tidak sama',
                'confirm_password.required' =>'Password tidak boleh kosong',
                'confirm_password.min'      =>'Password Minimal 6'
            ];

            $Validator = Validator::make($request->all(),[
                'email'             =>'required|email|exists:password_resets,email',
                'password'          =>'required|min:6|confirmed',
                'confirm_password'  =>'required|min:6'
            ],$message);

            if($Validator->passes()){
                $data = User::where('user_email', $request->email)->first();
                $update = User::where('user_email',$request->email)->update([
                'user_password'     => Hash::make($request->password),
                ]);

                $notif = new Notif;
                $notif->user_id     = $data->user_id;
                $notif->keterangan  = 'Password anda telah diubah';
                $notif->tanggal     = date('Y-m-d',time());
                $notif->flag_read   = '0';
                $notif->save();
                
                Auth::login($data);
                return redirect('/home');
            }
            else{
                $token = $request->token;
                $link = '/forgot/'+$token;
                return redirect($link)->withErrors($Validator)->withInput();
            }
        }
	}
}
