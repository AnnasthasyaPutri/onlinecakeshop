<?php

namespace App\Http\Controllers\System;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Address;
use Validator;
use Hash;
use Auth;


class RegisterController extends Controller
{
  public function register(Request $request){
    if(Auth::check() == true){
      if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
      }
      else{
          return redirect('/listproduct');
      }
    }
    else{
      $message = [
        'name.required'                   =>'User Name Tidak Boleh Kosong',
        'email.required'                  =>'Email Tidak Boleh Kosong',
        'email.unique'                    =>'Email Sudah Terdaftar',
        'email.email'                     =>'Format Email Salah',
        'phone.required'                  =>'No Telp tidak boleh kosong',
        'phone.numeric'                   =>'No telp harus angka',
        'phone.between'                   =>'No telp harus di antar 10 dan 13',
        'password.required'               =>'Password tidak boleh kosong',
        'password.min'                    =>'Password minimal 6 karakter',
        'password.confirmed'              =>'Password dan Confirm Password Tidak sama',
        'password_confirmation.required'  =>'Password tidak boleh kosong',
        'password_confirmation.min'       =>'Password Minimal 6 karakter'
      ];

      $Validator = Validator::make($request->all(),[
        'name'                  =>'required',
        'email'                 =>'required|unique:msuser,user_email|email',
        'phone'                 =>'required|between:9,14',
        'phone'                 =>'required|numeric',
        'password'              =>'required|min:6|confirmed',
        'password_confirmation' =>'required|min:6'
      ],$message);

      if($Validator->passes()){
        $data = new User;
        $data->user_name = $request->name;
        $data->user_email = $request->email;
        $data->user_phone = $request->phone;
        $data->user_password = Hash::make($request->password);
        $data->point = 0;
        $data->role_name = "user";
        $data->save();

        Auth::login($data);
        return redirect('/home');
      }
      else
      {
      return redirect()->back()->withErrors($Validator)->withInput();
      }
    }
  }
}
