<?php

namespace App\Http\Controllers\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Notif;
use App\Model\Password_Reset;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use App\Model\Kustom;
use Response;
use Hash;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;
use Auth;
use DateTime;
use Crypt;
use \Cart as Cart;

class CompareController extends Controller
{

  	public function choosecompareproduct($product){
  		$productId = Crypt::decrypt($product);
  		$prd = Product::where('product_id',$productId)->first();
  		$products = Product::where('flag_delete',0)->get();
		return view('User.viewproductcompare',compact('prd','products'));
  	}

  	public function compareproduct($product,$id){
  		$pId = Crypt::decrypt($product);
  		$productId = Crypt::decrypt($id);
  		$product1 = Product::where('product_id',$pId)->first();
  		$product2 = Product::where('product_id',$productId)->first();
  		$image1 = $product1->product_image;
  		$image2 = $product2->product_image;
  		// dd($image1, $image2);
  		// dd($product1,$product2);
		return view('User.resultcompareproduct',compact('product1','product2','image1','image2'));
  	}

  	public function comparecustom($kustom,$id){
  		$kustomId = Crypt::decrypt($kustom);
  		$kustom = Kustom::where('kustom_id',$kustomId)->first();
  		$productId = Crypt::decrypt($id);
  		$product = Product::where('product_id',$productId)->first();
  		// dd($kustom, $productId);
  		$image1 = $kustom->image;
  		$image2 = $product->product_image;
  		$base = BaseCake::where('base_cake_id',$kustom->base_cake_id)->value('base_cake_name');
  		$cover = CoverCake::where('cover_cake_id',$kustom->cover_cake_id)->value('cover_cake_name');
  		$decor = DekorasiCake::where('dekorasi_cake_id',$kustom->dekorasi_cake_id)->value('dekorasi_cake_name');
  		return view('User.resultcomparecustom',compact('product','kustom','base','cover','decor','image1','image2'));
  	}

  	public function choosecomparecustom($kustom){
  		$kustomId = Crypt::decrypt($kustom);
  		$kustom = Kustom::where('kustom_id',$kustomId)->first();
  		$products = Product::where('flag_delete',0)->get();
  		return view('User.viewproductcustom',compact('products','kustom'));
  	}
}
