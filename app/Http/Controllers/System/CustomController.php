<?php

namespace App\Http\Controllers\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Notif;
use App\Model\Password_Reset;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use App\Model\Kustom;
use Response;
use Hash;
use DB;
use Image;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;
use Auth;
use DateTime;
use Crypt;
use \Cart as Cart;

class CustomController extends Controller
{
	public function viewcustom($id){
		$productId = Crypt::decrypt($id);
    $product = Product::where('product_id',$productId)->first();
    $bases = BaseCake::where('flag_delete',0)->where('size_cake',$product->size_cake)->get();
    $covers = CoverCake::where('flag_delete',0)->where('size_cake',$product->size_cake)->get();
    $dekors = DekorasiCake::where('flag_delete',0)->get();
    $size = sizeof(Kustom::all())+1;
    // dd($size);
    $img = "product/".$product->product_image;
    $filename = 'Custom-0'.$size.'.png';
    $image = Image::make($img);
    $image->save('custom/'.$filename);
    

    $kustom = new Kustom;
    $kustom->base_cake_id = $product->base_cake_id;
    $kustom->cover_cake_id = $product->cover_cake_id;
    $kustom->dekorasi_cake_id = $product->dekorasi_cake_id;
    $kustom->calorie = $product->calorie;
    $kustom->price = $product->price;
    $kustom->size_cake = $product->size_cake;
    $kustom->flag_delete = 0;
    $kustom->image = $filename;
    $kustom->save();

    $basecake = BaseCake::where('base_cake_id',$product->base_cake_id)->first();
    $covercake = CoverCake::where('cover_cake_id',$product->cover_cake_id)->first();
    $dekor = DekorasiCake::where('dekorasi_cake_id',$product->dekorasi_cake_id)->first();
    $kustoms = Kustom::where('kustom_id',$size)->first();
    // dd($kustoms);
	return view('User.viewcustom',compact('kustoms','bases','covers','dekors','basecake','covercake','dekor'));
	}

  public function custom(Request $request){
    // dd($request);
    $custom = Kustom::where('kustom_id',$request->id)->first();
    $base = BaseCake::where('base_cake_id',$request->base)->first();
    $cover = CoverCake::where('cover_cake_id',$request->cover)->first();
    $dekor = DekorasiCake::where('dekorasi_cake_id',$request->dekor)->first();
  
    $image = Image::make('custom/'.$custom->image)->insert('cover/'.$cover->cover_image,'center')->insert('dekor/'.$dekor->dekorasi_image,'center');
    $image->save('custom/'.$custom->image);
    $price = $base->price+$cover->price+$dekor->price;
    $calorie = $base->calorie+$cover->calorie+$dekor->calorie;
    // $img = $custom->image;

    $kustom = Kustom::where('kustom_id',$request->id)->update([
      'base_cake_id' => $request->base,
      'cover_cake_id' => $request->cover,
      'dekorasi_cake_id' => $request->dekor,
      'price' => $price,
      'calorie' => $calorie,
    ]);
    session()->flash('success_message', 'Custom successfully!');

    $message = "Custom berhasil!";
    return response()->json(['success' => true, 'message' => $message, 'kalori' => $calorie, 'price' => $price]);
  }
}