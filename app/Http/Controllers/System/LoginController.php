<?php

namespace App\Http\Controllers\System;
use App\Model\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use \Cart as Cart;

class LoginController extends Controller
{
    public function index(){
        if(Auth::check() == true){
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                return redirect('/listproduct');
            }
        }
        else{
            $messageEmail = "";
            return view('User.login',compact('messageEmail'));
        }
    }
    
    public function login(Request $request){
        if(Auth::check() == true){
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                return redirect('/listproduct');
            }
        }
        else{
            $email      = $request->email;
            $password   = $request->password;
            $hasPass    = Hash::make($password);

            $users = User::where('user_email',$email)->first();
            // dd($users);

            if($users == true){
                if(Hash::check($password,$users->user_password)){
                    if($users->role_name == "user"){
                        Auth::login($users);
                        return redirect('/productdiameter22');
                    }
                    else if($users->role_name == "owner"){
                        Auth::login($users);
                        return redirect('/listproduct');
                    }
                }
                else{
                    $messageEmail = "Password anda salah.";
                    return view('User.login',compact('messageEmail'));
                }
            }
            else{
                $messageEmail = "Akun tidak terdaftar.";
                return view('User.login',compact('messageEmail'));
            }
        }
    }

    public function logout(){
        if(Auth::check() == false){
            return redirect('/productdiameter22');
        }
        else{
            Auth::logout();
           Cart::destroy();
            return redirect('/productdiameter22')->with('status3','Success');
        }   
    }

}
