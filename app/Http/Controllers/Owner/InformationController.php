<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DetailInformation;
use App\Model\User;
use Auth;
use Validator;

class InformationController extends Controller
{
  	public function index(){
        if(Auth::check() == false){
            return redirect('/productdiameter22');
        }
        else{
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                $info = DetailInformation::where('information_id','1')->first();
                $user = User::where('user_id',Auth::User()->user_id)->first();
                return view('Owner.information',compact('info','user'));
            }
        }
  	}

  	public function ubahprofile(){
        if(Auth::check() == false){
            return redirect('/productdiameter22');
        }
        else{
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                $info = DetailInformation::where('information_id','1')->first();
                $user = User::where('user_id',Auth::User()->user_id)->first();
                return view('Owner.ubahinfo',compact('info','user'));
            }
        }
  	}

  	public function ubah(Request $request){
        if(Auth::check() == false){
            return redirect('/productdiameter22');
        }
        else{
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                $message = [
                    'name.required'     =>'User Name Tidak Boleh Kosong',
                    'alamat.required'   =>'Alamat tidak boleh kosong',
                    'email.required'    =>'Email Tidak Boleh Kosong',
                    'email.email'       =>'Format Email Salah',
                    'phone.required'    =>'No Telp tidak boleh kosong',
                    'phone.numeric'     =>'No telp harus angka',
                    'phone.between'     =>'No telp harus di antar 10 dan 13',
                ];

                $Validator = Validator::make($request->all(),[
                    'name'      =>'required',
                    'alamat'    =>'required',
                    'email'     =>'required|email',
                    'phone'     =>'required|between:9,14',
                    'phone'     =>'required|numeric'
                ],$message);

                if($Validator->passes()){
                    $data = User::where('user_id',Auth::User()->user_id)->update([
                        'user_name'    => $request->name,
                        'user_email'   => $request->email,
                        'user_phone'   => $request->phone,
                    ]);

                    $info = DetailInformation::where('information_id','1')->update([
                        'alamat'               => $request->alamat,
                        'email'                => $request->email,
                        'contact_information'  => $request->phone,
                        'instagram'            => $request->ig,
                        'facebook'             => $request->fb,
                        'twitter'              => $request->twitter,
                    ]);
                    return redirect('/information');
                }
                else{
                    return redirect()->back()->withErrors($Validator)->withInput();
                }
            }
        }
    }

    public function ubahsandi(Request $request){
        if(Auth::check() == false){
            return redirect('/productdiameter22');
        }
        else{
            if(Auth::user()->role_name == "user"){
                return redirect('/productdiameter22');
            }
            else{
                $message = [
                    'password.required'                 =>'Password tidak boleh kosong',
                    'password.min'                      =>'Password minimal 6 karakter',
                    'password.confirmed'                =>'Password dan Confirm Password Tidak sama',
                    'password_confirmation.required'    =>'Password tidak boleh kosong',
                    'password_confirmation.min'         =>'Password Minimal 6 karakter'
                ];

                $Validator = Validator::make($request->all(),[
                    'password'              =>'required|min:6|confirmed',
                    'password_confirmation' =>'required|min:6'
                ],$message);

                if($Validator->passes()){
                    $data = User::where('user_id',Auth::User()->user_id)->first();
                    $data->user_password = Hash::make($request->password);
                    $data->save();

                  return redirect('/information');
                }
                else{
                    return redirect()->back()->withErrors($Validator)->withInput();
                }
            }
        }
    }
}
