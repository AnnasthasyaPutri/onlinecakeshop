<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Voucher;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class VoucherController extends Controller
{
  	public function index(){
  		if(Auth::check() == false){
  			return redirect('/productdiameter22');
  		}
  		else{
  			if(Auth::user()->role_name == "user"){
  				return redirect('/productdiameter22');
  			}
  			else{
		  		$vouchers = Voucher::all();
		  		return view('Owner.voucher.viewvoucher',compact('vouchers'));
  			}
  		}
  	}

  	public function viewadd(){
  		if(Auth::check() == false){
  			return redirect('/productdiameter22');
  		}
  		else{
  			if(Auth::user()->role_name == "user"){
  				return redirect('/productdiameter22');
  			}
  			else{		
		  		$products = Product::where('flag_delete',0)->get();
		  		$bases = BaseCake::where('flag_delete',0)->get();
		  		$covers = CoverCake::where('flag_delete',0)->get();
		  		$decors = DekorasiCake::where('flag_delete',0)->get();
		  		return view('Owner.voucher.viewaddvoucher',compact('products','bases','covers','decors'));
  			}
  		}
  	}

  	public function addvoucher(Request $request){
  		if(Auth::check() == false){
  			return redirect('/productdiameter22');
  		}
  		else{
  			if(Auth::user()->role_name == "user"){
  				return redirect('/productdiameter22');
  			}
  			else{	
		  		// dd($request);
		  		$message = [
			      'name.required'		=> 'Nama Kupon tidak boleh kosong',
			      'tipe.required'		=> 'Tipe Kupon tidak boleh kosong',
			      'detail.required'		=> 'Detail Kupon tidak boleh kosong',
			      'poin.required'		=> 'Poin Kupon tidak boleh kosong',
			      'qty.required'		=> 'Quantity Kupon tidak boleh kosong',
			      'minimum.required'	=> 'Minimum Jumlah tidak boleh kosong',
			      'start.required'		=> 'Tanggal Awal tidak boleh kosong',
			      'expired.required'	=> 'Tanggal Akhir tidak boleh kosong',
			    ];
			      
			    $validator = Validator::make($request->all(),[
			      'name' 	=> 'required',
			      'tipe' 	=> 'required',
			      'detail' 	=> 'required',
			      'poin' 	=> 'required',
			      'qty' 	=> 'required',
			      'minimum' => 'required',
			      'start' 	=> 'required',
			      'expired' => 'required',
			    ],$message);

			    if($validator->fails()){
			    	return redirect()->back()->withErrors($validator)->withInput();
			    }
			    else{
			    	if($request->persen != null){
			    		$percent = $request->persen / 100;
			    	}
			    	else{
			    		$percent = $request->persen;
			    	}
			    	$voucher = New Voucher;
			    	$voucher->voucher_type = $request->tipe;
			    	$voucher->voucher_name = $request->name;
			    	$voucher->voucher_detail = $request->detail;
			    	$voucher->minimum = $request->minimum;
			    	$voucher->discount = $request->discount;
			    	$voucher->percent = $percent;
			    	$voucher->qty = $request->qty;
			    	$voucher->start_date = $request->start;
			    	$voucher->expired_date = $request->expired;
			    	$voucher->point = $request->poin;
			    	$voucher->flag_delete = 0;
			    	$voucher->product_id = $request->product;
			    	$voucher->base_cake_id = $request->base;
			    	$voucher->cover_cake_id = $request->cover;
			    	$voucher->decor_cake_id = $request->decor;
			    	$voucher->save();
			    	return redirect('/listvoucher')->with('statusok','Success');
			    }
  			}
  		}
  	}

  	public function viewupdate($id){
  		if(Auth::check() == false){
  			return redirect('/productdiameter22');
  		}
  		else{
  			if(Auth::user()->role_name == "user"){
  				return redirect('/productdiameter22');
  			}
  			else{
		  		$voucherId = Crypt::decrypt($id);
		  		$voucher = Voucher::where('voucher_id',$voucherId)->first();
		  		$products = Product::where('flag_delete',0)->get();
		  		$bases = BaseCake::where('flag_delete',0)->get();
		  		$covers = CoverCake::where('flag_delete',0)->get();
		  		$decors = DekorasiCake::where('flag_delete',0)->get();
		  		return view('Owner.voucher.viewupdatevoucher',compact('voucher','products','bases','covers','decors'));
  			}
  		}
  	}

  	public function updatevoucher(Request $request){
  		if(Auth::check() == false){
  			return redirect('/productdiameter22');
  		}
  		else{
  			if(Auth::user()->role_name == "user"){
  				return redirect('/productdiameter22');
  			}
  			else{
		  		// dd($request);
		  		$message = [
			      'name.required'		=> 'Nama Kupon tidak boleh kosong',
			      'tipe.required'		=> 'Tipe Kupon tidak boleh kosong',
			      'detail.required'		=> 'Detail Kupon tidak boleh kosong',
			      'poin.required'		=> 'Poin Kupon tidak boleh kosong',
			      'qty.required'		=> 'Quantity Kupon tidak boleh kosong',
			      'minimum.required'	=> 'Minimum Jumlah tidak boleh kosong',
			      'start.required'		=> 'Tanggal Awal tidak boleh kosong',
			      'expired.required'	=> 'Tanggal Akhir tidak boleh kosong',
			    ];
			      
			    $validator = Validator::make($request->all(),[
			      'name' 	=> 'required',
			      'tipe' 	=> 'required',
			      'detail' 	=> 'required',
			      'poin' 	=> 'required',
			      'qty' 	=> 'required',
			      'minimum' => 'required',
			      'start' 	=> 'required',
			      'expired' => 'required',
			    ],$message);

			    if($validator->fails()){
			    	return redirect()->back()->withErrors($validator)->withInput();
			    }
			    else{
			    	if($request->persen != null){
			    		$percent = $request->persen / 100;
			    	}
			    	else{
			    		$percent = null;
			    	}
			    	
			    	$voucher = Voucher::where('voucher_id',$request->voucherId)->first();
			  		if($request->product != 0){
						$product = $request->product;
					}
					else{
						$product = $voucher->product_id;
					}

					if($request->base != 0){
						$base = $request->base;
					}
					else{
						$base = $voucher->base_cake_id;
					}

					if($request->cover != 0){
						$cover = $request->cover;
					}
					else{
						$cover = $voucher->cover_cake_id;
					}

					if($request->decor != 0){
						$decor = $request->decor;
					}
					else{
						$decor = $voucher->decor_cake_id;
					}

			  		$voucher->voucher_type = $request->tipe;
			    	$voucher->voucher_name = $request->name;
			    	$voucher->voucher_detail = $request->detail;
			    	$voucher->minimum = $request->minimum;
			    	$voucher->discount = $request->discount;
			    	$voucher->percent = $percent;
			    	$voucher->qty = $request->qty;
			    	$voucher->start_date = $request->start;
			    	$voucher->expired_date = $request->expired;
			    	$voucher->point = $request->poin;
			    	$voucher->product_id = $product;
			    	$voucher->base_cake_id = $base;
			    	$voucher->cover_cake_id = $cover;
			    	$voucher->decor_cake_id = $decor;
			    	$voucher->save();
			    	return redirect('/listvoucher')->with('statusupdate','Success');
			    }
  			}
  		}
  	}

  	public function delete($id){
  		if(Auth::check() == false){
  			return redirect('/productdiameter22');
  		}
  		else{
  			if(Auth::user()->role_name == "user"){
  				return redirect('/productdiameter22');
  			}
  			else{
  				$voucherId = Crypt::decrypt($id);
				$voucher = Voucher::where('voucher_id',$voucherId)->forcedelete();
				return redirect('/listvoucher')->with('statushapus','Success');
  			}
  		}
	}
}
