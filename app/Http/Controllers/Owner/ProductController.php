<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class ProductController extends Controller
{
	public function index(){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/productdiameter22');
      }
      else{
        $products = Product::where('flag_delete',0)->get();
        return view('Owner.product.viewproduct',compact('products'));
      }
    }
	}

	public function viewadd(){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/productdiameter22');
      }
      else{
        $base = BaseCake::where('flag_delete',0)->get();
        $cover = CoverCake::where('flag_delete',0)->get();
        $dekor = DekorasiCake::where('flag_delete',0)->get();
        return view('Owner.product.viewaddproduct',compact('base','cover','dekor'));
      }
    }
	}

	public function addproduct(Request $request){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/productdiameter22');
      }
      else{
        // dd($request->all());
        $message = [
          'name.required'   => 'Nama produk tidak boleh kosong',
          'base.required'   => 'Kue dasar tidak boleh kosong',
          'cover.required'  => 'Lapisan kue tidak boleh kosong',
          'dekor.required'  => 'Dekorasi kue tidak boleh kosong',
        ];
          
        $validator = Validator::make($request->all(),[
          'name'    => 'required',
          'base'    => 'required',
          'cover'   => 'required',
          'dekor'   => 'required',
        ],$message);

        $imagevalidator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png,gif|max:10000',
        ],$message);

        if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
          $filename = "";
          $path ='product/';
            // dd($request->file('image'));
          $image = $request->file('image');

          if($request->hasfile('image')){
            if($imagevalidator->fails()){
              return redirect()->back()->withErrors($imagevalidator)->withInput();
            }
            else{
              $base = BaseCake::where('base_cake_id',$request->base)->first();
              $cover = CoverCake::where('cover_cake_id',$request->cover)->first();
              $dekor = DekorasiCake::where('dekorasi_cake_id',$request->dekor)->first();
              $price = $base->price + $cover->price + $dekor->price;
              $calorie = $base->calorie + $cover->calorie + $dekor->calorie;

              if($base->size_cake != $cover->size_cake){
                return redirect()->back()->with('fail','gagal');
              }
              else{
                $filename = $image->getClientOriginalName();
                $image->move($path, $filename);
                
                $product = new Product;
                $product->product_name      = $request->name;
                $product->base_cake_id      = $request->base;
                $product->size_cake         = $base->size_cake;
                $product->cover_cake_id     = $request->cover;
                $product->dekorasi_cake_id  = $request->dekor;
                $product->price             = $price;
                $product->calorie           = $calorie;
                $product->product_image     = $filename;
                $product->flag_populer      = 0;
                $product->flag_delete       = 0;
                $product->save();

                return redirect('/listproduct')->with('statusok','Success');
              }
            }
          }
        }
      }
    }
  }

  public function viewupdate($id){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/productdiameter22');
      }
      else{
        $productId = Crypt::decrypt($id);
        $product = Product::where('product_id',$productId)->first();
        $base = BaseCake::where('size_cake',$product->size_cake)->where('flag_delete',0)->get();
        $cover = CoverCake::where('size_cake',$product->size_cake)->where('flag_delete',0)->get();
        $dekor = DekorasiCake::where('flag_delete',0)->get();
        return view('Owner.product.viewupdateproduct',compact('product','base','cover','dekor'));
      }
    }
  }

  public function updateproduct(Request $request){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/productdiameter22');
      }
      else{
        $message = [
          'name.required'   => 'Nama produk tidak boleh kosong',
          'base.required'   => 'Kue dasar tidak boleh kosong',
          'cover.required'  => 'Lapisan kue tidak boleh kosong',
          'dekor.required'  => 'Dekorasi kue tidak boleh kosong',
        ];

        $validator = Validator::make($request->all(),[
          'name'    => 'required',
          'base'    => 'required',
          'cover'   => 'required',
          'dekor'   => 'required',
        ],$message);

        $imagevalidator = Validator::make($request->all(),[
          'image' => 'mimes:jpeg,jpg,png,gif|max:100000',
        ],$message);

        if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
          // dd($request->file('image'), Product::where('product_id',$request->productId)->value('product_image'));
          $filename = "";
          $pic = "";
          $path ='product/';
          $image = $request->file('image');

          if($request->hasfile('image')){
            if($imagevalidator->fails()){
              return redirect()->back()->withErrors($imagevalidator)->withInput();
            }
            else{
              $filename = $image->getClientOriginalName();
              $image->move($path, $filename);
              $pic = $filename;

            }
          }
          else{
            $pic = Product::where('product_id',$request->productId)->value('product_image');
          }

          $base = BaseCake::where('base_cake_id',$request->base)->first();
          $cover = CoverCake::where('cover_cake_id',$request->cover)->first();
          $dekor = DekorasiCake::where('dekorasi_cake_id',$request->dekor)->first();
          $price = $base->price + $cover->price + $dekor->price;
          $calorie = $base->calorie + $cover->calorie + $dekor->calorie;

          $product = Product::where('product_id',$request->productId)->first();
          $product->product_name = $request->name;
          $product->product_image   = $pic;
          $product->price       = $price;
          $product->calorie     = $calorie;
          $product->flag_populer  = 0;
          $product->save();

          return redirect('/listproduct')->with('statusupdate','Success');
        }
      }
    }
  }

	public function deleteproduct($id){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/productdiameter22');
      }
      else{
        $productId = Crypt::decrypt($id);
        $product = Product::where('product_id',$productId)->update([
          'flag_delete' => 1,
        ]);
        return redirect('/listproduct')->with('statushapus','Success');
      }
    }
	}
}
