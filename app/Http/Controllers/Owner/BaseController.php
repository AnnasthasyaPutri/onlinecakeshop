<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class BaseController extends Controller
{
  	public function index(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $bases = BaseCake::where('flag_delete',0)->get();
          return view('Owner.base.viewbasecake',compact('bases'));
        }
      }
  	}

  	public function viewadd(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          return view('Owner.base.viewaddbase');
        }
      }
  	}

  	public function addbasecake(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $message = [
            'name.required'   => 'Nama kue tidak boleh kosong',
            'size.required'   => 'Ukuran kue tidak boleh kosong',
            'price.required'  => 'Harga tidak boleh kosong',
            'calorie.required'  => 'Kalori tidak boleh kosong',
          ];
          $validator = Validator::make($request->all(),[
            'name'    => 'required',
              'size'    => 'required',
              'price'   => 'required',
              'calorie'   => 'required',
          ],$message);

          if($validator->fails()){
            return redirect()->back()
                          ->withErrors($validator)
                          ->withInput();
          }
          else{
              $base = new BaseCake;
              $base->base_cake_name = $request->name;
              $base->size_cake    = $request->size;
          $base->price      = $request->price;
          $base->calorie      = $request->calorie;
          $base->flag_populer   = 0;
          $base->flag_delete    = 0;
          $base->save();

          return redirect('/listbasecake')->with('statusok','Success');
          }
        }
      }
  	}

  	public function viewupdate($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $baseId = Crypt::decrypt($id);
          $base = BaseCake::where('base_cake_id',$baseId)->first();
          return view('Owner.base.viewupdatebase',compact('base'));
        }
      }
  	}

  	public function updatebasecake(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $message = [
                'name.required'   => 'Nama kue tidak boleh kosong',
                'size.required'   => 'Ukuran kue tidak boleh kosong',
                'price.required'  => 'Harga tidak boleh kosong',
                'calorie.required'  => 'Kalori tidak boleh kosong',
            ];
            $validator = Validator::make($request->all(),[
              'name'    => 'required',
                'size'    => 'required',
                'price'   => 'required',
                'calorie'   => 'required',
            ],$message);

            if($validator->fails()){
              return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            else{
                $base = BaseCake::where('base_cake_id',$request->baseId)->first();
                $base->base_cake_name = $request->name;
                $base->size_cake    = $request->size;
            $base->price      = $request->price;
            $base->calorie      = $request->calorie;
            $base->flag_populer   = 0;
            $base->save();

                return redirect('/listbasecake')->with('statusupdate','Success');
            }
        }
      }
  	}

  	public function deletebasecake($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $baseId = Crypt::decrypt($id);
          $base = BaseCake::where('base_cake_id',$baseId)->update([
            'flag_delete' => 1,
          ]);
          return redirect('/listbasecake')->with('statushapus','Success');
        }
      }
  	}
}
