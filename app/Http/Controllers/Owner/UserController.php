<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Voucher;
use App\Model\VoucherUser;
use App\Model\Notif;
use Illuminate\Support\Facades\Mail;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class UserController extends Controller
{
  	public function index(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $date = date('d-m-Y');
          $users = User::where('role_name','user')->get();
          $vouchers = Voucher::where('flag_delete',0)->where('expired_date','>',$date)->get();
          return view('Owner.user.viewuser',compact('users','vouchers'));
        }
      }
  	}

  	public function sendvoucher(Request $request, $id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          // dd($request);
          $userId = Crypt::decrypt($id);
          $voucher = Voucher::where('voucher_id',$request->voucher)->first();
          $user = User::where('user_id',$userId)->first();

          $newvoucher = new VoucherUser;
          $newvoucher->voucher_id = $voucher->voucher_id;
          $newvoucher->user_id = $user->user_id;
          $newvoucher->flag_delete = 0;
          $newvoucher->save();

          $voucher->qty = $voucher->qty-1;
          $voucher->save();

          $data = [];

          try{
            $data['name'] = $user->user_name;
            $email = $user->user_email;
            $data['path']= url('/yourvoucher');

            Mail::send('Mail.voucher', $data, function ($message) use($email) {
                      $message->to($email)->subject('Kamu Mendapatkan Kupon');
            });
          }
          catch(Exception $e){
          }

          $notif = new Notif;
          $notif->user_id = $userId;
          $notif->keterangan = "Anda baru mendapatkan voucher";
          $notif->tanggal = \Carbon\Carbon::now();
          $notif->subject = "Kiriman Voucher";
          $notif->save();

          return redirect('/listdatauser')->with('kirim','Success');
        }
      }
  	}
}
