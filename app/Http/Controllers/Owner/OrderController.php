<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Model\Notif;
use App\Model\User;
use App\Model\Voucher;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use App\Model\HeaderTransaction;
use App\Model\DetailTransaction;
use App\Model\DataPembayaran;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class OrderController extends Controller
{
  	public function index(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          $order = HeaderTransaction::where('transaction_status','<',3)->get();
          // dd($order);
          foreach($order as $ord){
            if($ord->pick_date < \Carbon\Carbon::now()->toDateString()){
              if($ord->status_payment == 0){
                $trsc = HeaderTransaction::where('transaction_header_id',$ord->transaction_header_id)->first();
                $trsc->transaction_status = 4;
                $trsc->save();

                if($trsc->user_id != null){
                  $notif = new Notif;
                  $notif->user_id = $trsc->user_id;
                  $notif->subject = "Pesanan ".$trsc->transaction_code;
                  $notif->keterangan = "Pesanan anda dibatalkan";
                  $notif->tanggal = \Carbon\Carbon::now();
                  $notif->save();

                  try{
                    $data['name'] = $trsc->user_name;
                    $data['status'] = "Pesanan kamu dengan kode ".$trsc->transaction_code." di batalkan. Silahkan lihat perkembangannya ";
                    $email = $trsc->user_email;
                    $data['path']= url('/historyorder');

                    Mail::send('Mail.notification', $data, function ($message) use($email) {
                              $message->to($email)->subject('Notifikasi Pesanan');
                    });
                  }
                  catch(Exception $e){
                  }
                }
                else{
                  try{
                    $email = $trsc->user_email;
                    $data['status'] = "Pesanan kamu dengan kode ".$trsc->transaction_code." di batalkan.";

                    Mail::send('Mail.guest', $data, function ($message) use($email) {
                              $message->to($email)->subject('Notifikasi Pesanan');
                    });
                  }
                  catch(Exception $e){
                  }
                }
              }
              else{
                $order = HeaderTransaction::where('transaction_header_id',$trscid)->first();
                $price = $order->total_price;
                $poin = substr($price,0,2);

                if($order->user_id != null){
                  $ordr = HeaderTransaction::where('transaction_header_id',$trscid)->update([
                    'transaction_status' => 3,
                  ]);

                  $user = User::where('user_id', $order->user_id)->first();
                  $point = $user->point+$poin;
                  $usr = User::where('user_id', $order->user_id)->update([
                    'point' => $point,
                  ]);

                  $notif = new Notif;
                  $notif->user_id = $order->user_id;
                  $notif->subject = "Pesanan ".$order->transaction_code;
                  $notif->keterangan = "Pesanan anda sudah diambil";
                  $notif->tanggal = \Carbon\Carbon::now();
                  $notif->save();

                  try{
                    $data['name'] = $order->user_name;
                    $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." sudah diambil. Silahkan lihat perkembangannya ";
                    $email = $order->user_email;
                    $data['path']= url('/historyorder');

                    Mail::send('Mail.notification', $data, function ($message) use($email) {
                              $message->to($email)->subject('Notifikasi Pesanan');
                    });
                  }
                  catch(Exception $e){
                  }
                }
                else{
                  $ordr = HeaderTransaction::where('transaction_header_id',$trscid)->update([
                    'transaction_status' => 3,
                  ]);
                  
                  try{
                    $email = $order->user_email;
                    $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." sudah diambil.";

                    Mail::send('Mail.guest', $data, function ($message) use($email) {
                              $message->to($email)->subject('Notifikasi Pesanan');
                    });
                  }
                  catch(Exception $e){
                  }
                }
              }
            }
            else{}
          }
          $orders = HeaderTransaction::orderBy('pick_date','asc')->whereDate('pick_date','>=',\Carbon\Carbon::now()->toDateString())->where('transaction_status','!=',4)->get();
          return view('Owner.order.vieworder',compact('orders'));
        }
      }
  	}

  	public function cancel($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          $trscid = Crypt::decrypt($id);
          $order = HeaderTransaction::where('transaction_header_id',$trscid)->first();
          $order->transaction_status = 4;
          $order->save();

          if($order->user_id != null){
            $notif = new Notif;
            $notif->user_id = $order->user_id;
            $notif->subject = "Pesanan ".$order->transaction_code;
            $notif->keterangan = "Pesanan anda dibatalkan";
            $notif->tanggal = \Carbon\Carbon::now();
            $notif->save();

            try{
              $data['name'] = $trsc->user_name;
              $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." di batalkan. Silahkan lihat perkembangannya ";
              $email = $order->user_email;
              $data['path']= url('/historyorder');

              Mail::send('Mail.notification', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          else{
            try{
              $email = $order->user_email;
              $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." di batalkan.";

              Mail::send('Mail.guest', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          return redirect('/listorder')->with('statuscancel','Success');
        }
      }
  	}

  	public function pickup($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          $trscid = Crypt::decrypt($id);
          $order = HeaderTransaction::where('transaction_header_id',$trscid)->first();
          $price = $order->total_price;
          $poin = substr($price,0,2);
          

          if($order->user_id != null){
            $ordr = HeaderTransaction::where('transaction_header_id',$trscid)->update([
              'transaction_status' => 3,
            ]);

            $user = User::where('user_id', $order->user_id)->first();
            $point = $user->point+$poin;
            $usr = User::where('user_id', $order->user_id)->update([
              'point' => $point,
            ]);

            $notif = new Notif;
            $notif->user_id = $order->user_id;
            $notif->subject = "Pesanan ".$order->transaction_code;
            $notif->keterangan = "Pesanan anda sudah diambil";
            $notif->tanggal = \Carbon\Carbon::now();
            $notif->save();

            try{
              $data['name'] = $order->user_name;
              $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." sudah diambil. Silahkan lihat perkembangannya ";
              $email = $order->user_email;
              $data['path']= url('/historyorder');

              Mail::send('Mail.notification', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          else{
            $ordr = HeaderTransaction::where('transaction_header_id',$trscid)->update([
              'transaction_status' => 3;
            ]);

            try{
              $email = $order->user_email;
              $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." sudah diambil.";

              Mail::send('Mail.guest', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          return redirect('/listorder')->with('statuspickup','Success');
        }
      }
  	}

  	public function update(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          if($request->pick_date <= \Carbon\Carbon::now()->toDateString()){
            return redirect()->back()->with('statusbatalupdate','Success');
          }
          $order = HeaderTransaction::where('transaction_header_id',$request->trscid)->first();
          $order->pick_date = $request->pick_date;
          $order->save();

          $user = HeaderTransaction::where('transaction_header_id',$request->trscid)->value('user_id');
          $code = HeaderTransaction::where('transaction_header_id',$request->trscid)->value('transaction_code');

          if($order->user_id != null){
            $notif = new Notif;
            $notif->user_id = $user;
            $notif->subject = "Pesanan ".$code;
            $notif->keterangan = "Pesanan anda diganti tanggal pengambilan";
            $notif->tanggal = \Carbon\Carbon::now();
            $notif->save();

            try{
              $data['name'] = $order->user_name;
              $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." tanggal pengambilan diubah. Silahkan lihat perkembangannya ";
              $email = $order->user_email;
              $data['path']= url('/historyorder');

              Mail::send('Mail.notification', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          else{
            try{
              $email = $order->user_email;
              $data['status'] = "Pesanan kamu dengan kode ".$order->transaction_code." tanggal pengambilan diubah.";

              Mail::send('Mail.guest', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          return redirect('/listorder')->with('statusupdate','Success');
        }
      }
  	}

  	public function approved(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          $transaksi = HeaderTransaction::where('transaction_header_id',$request->id)->first();
          $transaksi->transaction_status = 2;
          $transaksi->save();

          if($transaksi->user_id != null){
            $notif = new Notif;
            $notif->user_id = $transaksi->user_id;
            $notif->subject = "Pesanan ".$transaksi->transaction_code;
            $notif->keterangan = "Pembayaran pesanan anda sudah di terima";
            $notif->tanggal = \Carbon\Carbon::now();
            $notif->save();

            try{
              $data['name'] = $transaksi->user_name;
              $data['status'] = "Pesanan kamu dengan kode ".$transaksi->transaction_code." sudah diambil. Silahkan lihat perkembangannya ";
              $email = $transaksi->user_email;
              $data['path']= url('/historyorder');

              Mail::send('Mail.notification', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          else{
            try{
              $email = $transaksi->user_email;
              $data['status'] = "Pesanan kamu dengan kode ".$transaksi->transaction_code." sudah diambil.";

              Mail::send('Mail.guest', $data, function ($message) use($email) {
                        $message->to($email)->subject('Notifikasi Pesanan');
              });
            }
            catch(Exception $e){
            }
          }
          return redirect('/listorder')->with('statuspayment','Success');
        }
      }
  	}

  	public function viewconfirmation($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $trscid = Crypt::decrypt($id);
          $order = HeaderTransaction::where('transaction_header_id',$trscid)->first();
          return view('Owner.order.viewpaymentconfirmation',compact('order'));
        }
      }
  	}

  	public function downloadfile($id) {
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{

          $decrypt = Crypt::decrypt($id);
          $data = DataPembayaran::where('transaction_header_id', $decrypt)->first();
          // dd($decrypt, $data);
          $path = "payment/BP-".$data->headertransaction->transaction_code;

          $myFile = public_path($path);
          return response()->download($myFile);
        }
      }
    }

  	public function rememberpayment($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $trscid = Crypt::decrypt($id);
          $trsc = HeaderTransaction::where('transaction_header_id',$trscid)->first();
          // dd($trsc);
          $data = [];

            try{
              $data['kode'] = $trsc->transaction_code;
              $email = $trsc->user_email;
              $data['path']= url('/paymentconfirmation');

              Mail::send('Mail.rememberpayment', $data, function ($message) use($email) {
                        $message->to($email)->subject('Anda belum melakukan pembayaran');
              });
            }
            catch(Exception $e){
            }

          if($trsc->user_id != null){
            $notif = new Notif;
            $notif->user_id = $trsc->user_id;
            $notif->subject = "Pesanan ".$trsc->transaction_code;
            $notif->keterangan = "Pesanan anda belum di bayar";
            $notif->tanggal = \Carbon\Carbon::now();
            $notif->save();
          }
          else{

          }
          return redirect()->back()->with('statusingat','Success');
        }
      }
  	}

  	public function viewbuyer($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $trscid = Crypt::decrypt($id);
          $trsc = HeaderTransaction::where('transaction_header_id',$trscid)->first();
          return view('Owner.order.userinfo',compact('trsc'));
        }
      }
    }

  	public function orderdetail($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{      
          $trscid = Crypt::decrypt($id);
          $orders = DetailTransaction::where('transaction_header_id',$trscid)->get();
          return view('Owner.order.orderdetail',compact('orders'));
        }
      }
  	}
}
