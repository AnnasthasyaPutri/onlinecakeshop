<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class CoverController extends Controller
{
  	public function index(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          $covers = CoverCake::where('flag_delete',0)->get();
          return view('Owner.cover.viewcovercake',compact('covers'));
        }
      }
  	}

  	public function viewadd(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          return view('Owner.cover.viewaddcover');
        }
      }
  	}

  	public function addcovercake(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{ 
          // dd($request);
          $message = [
                'name.required'   => 'Nama kue tidak boleh kosong',
                'size.required'   => 'Ukuran kue tidak boleh kosong',
                'price.required'  => 'Harga tidak boleh kosong',
                'calorie.required'  => 'Kalori tidak boleh kosong',
            ];
            $validator = Validator::make($request->all(),[
              'name'    => 'required',
                'size'    => 'required',
                'price'   => 'required',
                'calorie'   => 'required',
            ],$message);

            $imagevalidator = Validator::make($request->all(),[
                'image' => 'mimes:jpeg,jpg,png,gif|max:100000',
            ],$message);

            if($validator->fails()){
              return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }
            else{
              $filename = "";
              $path ='cover/';
              // dd($request->file('image'));
          $image = $request->file('image');
                  
            if($request->hasfile('image')){
                  if($imagevalidator->fails()){
                      return redirect()
                      ->back()
                      ->withErrors($imagevalidator)
                      ->withInput();
                  }
                  else{
                       $filename = $image->getClientOriginalName();
                        $image->move($path, $filename);

                  }
              }

                $cover = new CoverCake;
                $cover->cover_cake_name = $request->name;
                $cover->size_cake   = $request->size;
            $cover->price       = $request->price;
            $cover->calorie     = $request->calorie;
            $cover->cover_image   = $filename;
            $cover->flag_populer  = 0;
            $cover->flag_delete   = 0;
            $cover->save();

            return redirect('/listcovercake')->with('statusok','Success');
            }
        }
      }
  	}

  	public function viewupdate($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $coverId = Crypt::decrypt($id);
          $cover = CoverCake::where('cover_cake_id',$coverId)->first();
          return view('Owner.cover.viewupdatecover',compact('cover'));
        }
      }
  	}

  	public function updatecovercake(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{      
          // dd($request, $request->file('image'));
          $message = [
                'name.required'   => 'Nama kue tidak boleh kosong',
                'size.required'   => 'Ukuran kue tidak boleh kosong',
                'price.required'  => 'Harga tidak boleh kosong',
                'calorie.required'  => 'Kalori tidak boleh kosong',
            ];
            $validator = Validator::make($request->all(),[
              'name'    => 'required',
                'size'    => 'required',
                'price'   => 'required',
                'calorie'   => 'required',
            ],$message);

            $imagevalidator = Validator::make($request->all(),[
                'image' => 'mimes:jpeg,jpg,png,gif|max:100000',
            ],$message);

            if($validator->fails()){
              return redirect()->back()->withErrors($validator)->withInput();
            }
            else{
              $filename = "";
              $pic = "";
              $path ='cover/';
          $image = $request->file('image');
                  
            if($request->hasfile('image')){
                  if($imagevalidator->fails()){
                      return redirect()
                      ->back()
                      ->withErrors($imagevalidator)
                      ->withInput();
                  }
                  else{
                    $filename = $image->getClientOriginalName();
                      $image->move($path, $filename);
                      $pic = $filename;

                  }
              }
              else{
                $pic = CoverCake::where('cover_cake_id',$request->coverId)->value('cover_image');
              }

              // dd($request->file('image'), $request->hasfile('image'), $pic, CoverCake::where('cover_cake_id',$request->coverId)->value('cover_image'));

                $cover = CoverCake::where('cover_cake_id',$request->coverId)->first();
                $cover->cover_cake_name = $request->name;
                $cover->size_cake   = $request->size;
                $cover->cover_image   = $pic;
            $cover->price       = $request->price;
            $cover->calorie     = $request->calorie;
            $cover->flag_populer  = 0;
            $cover->save();

                return redirect('/listcovercake')->with('statusupdate','Success');
            }
        }
      }
  	}

  	public function deletecovercake($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/productdiameter22');
        }
        else{
          $coverId = Crypt::decrypt($id);
          $cover = CoverCake::where('cover_cake_id',$coverId)->update([
            'flag_delete' => 1,
          ]);
          return redirect('/listcovercake')->with('statushapus','Success');
        }
      }
  	}
}
