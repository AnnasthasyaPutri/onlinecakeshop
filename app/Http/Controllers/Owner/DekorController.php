<?php

namespace App\Http\Controllers\Owner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Product;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use Auth;
use Crypt;
use Validator;
use Session;
use DateTime;
use Response;

class DekorController extends Controller
{
  	public function index(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/product_diameter22');
        }
        else{
                $decors = DekorasiCake::where('flag_delete',0)->get();
      return view('Owner.decor.viewdecorcake',compact('decors'));
        }
      }
  	}

  	public function viewadddecor(){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/product_diameter22');
        }
        else{
                return view('Owner.decor.viewadddecor');
        }
      }
  	}

	public function adddecorationcake(Request $request){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/product_diameter22');
      }
      else{
        $message = [
            'name.required'   => 'Nama kue tidak boleh kosong',
            'price.required'  => 'Harga tidak boleh kosong',
            'calorie.required'  => 'Kalori tidak boleh kosong',
        ];
        $validator = Validator::make($request->all(),[
          'name'    => 'required',
            'price'   => 'required',
            'calorie'   => 'required',
        ],$message);

        $imagevalidator = Validator::make($request->all(),[
            'image' => 'mimes:jpeg,jpg,png,gif|max:100000',
        ],$message);

        if($validator->fails()){
          return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
          $filename = "";
          $path ='dekor/';
          // dd($request->file('image'));
      $image = $request->file('image');
              
        if($request->hasfile('image')){
              if($imagevalidator->fails()){
                  return redirect()->back()->withErrors($imagevalidator)->withInput();
              }
              else{
                   $filename = $image->getClientOriginalName();
                    $image->move($path, $filename);

              }
          }

            $decor = new DekorasiCake;
            $decor->dekorasi_cake_name  = $request->name;
        $decor->price         = $request->price;
        $decor->calorie       = $request->calorie;
        $decor->dekorasi_image    = $filename;
        $decor->flag_populer    = 0;
        $decor->flag_delete     = 0;
        $decor->save();

        return redirect('/listdecorcake')->with('statusok','Success');
        }
      }
    }
  }

	public function viewupdatedecor($id){
    if(Auth::check() == false){
      return redirect('/productdiameter22');
    }
    else{
      if(Auth::user()->role_name == "user"){
        return redirect('/product_diameter22');
      }
      else{
        $decorId = Crypt::decrypt($id);
        $decor = DekorasiCake::where('dekorasi_cake_id',$decorId)->first();
        return view('Owner.decor.viewupdatedecor',compact('decor'));
      }
    }
  }

  	public function updatedecorationcake(Request $request){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/product_diameter22');
        }
        else{
          $message = [
            'name.required'   => 'Nama kue tidak boleh kosong',
            'price.required'  => 'Harga tidak boleh kosong',
            'calorie.required'  => 'Kalori tidak boleh kosong',
          ];
          $validator = Validator::make($request->all(),[
            'name'    => 'required',
              'price'   => 'required',
              'calorie'   => 'required',
          ],$message);

          $imagevalidator = Validator::make($request->all(),[
              'image' => 'mimes:jpeg,jpg,png,gif|max:100000',
          ],$message);

          if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
          }
          else{
            $filename = "";
            $pic = "";
            $path ='dekor/';
        $image = $request->file('image');
                
          if($request->hasfile('image')){
                if($imagevalidator->fails()){
                    return redirect()
                    ->back()
                    ->withErrors($imagevalidator)
                    ->withInput();
                }
                else{
                  $filename = $image->getClientOriginalName();
                    $image->move($path, $filename);
                    $pic = $filename;

                }
            }
            else{
              $pic = DekorasiCake::where('dekorasi_cake_id',$request->decorId

            )->value('cover_image');
            }

            // dd($request->file('image'), $request->hasfile('image'), $pic, CoverCake::where('cover_cake_id',$request->coverId)->value('cover_image'));

              $decor = DekorasiCake::where('dekorasi_cake_id',$request->decorId)->first();
              $decor->dekorasi_cake_name  = $request->name;
          $decor->price         = $request->price;
          $decor->calorie       = $request->calorie;
          $decor->dekorasi_image    = $pic;
          $decor->flag_populer    = 0;
          $decor->save();

              return redirect('/listdecorcake')->with('statusupdate','Success');
          }
        }
      }
  	}

  	public function deletedecorcake($id){
      if(Auth::check() == false){
        return redirect('/productdiameter22');
      }
      else{
        if(Auth::user()->role_name == "user"){
          return redirect('/product_diameter22');
        }
        else{
          $decorId = Crypt::decrypt($id);
          $decor = DekorasiCake::where('dekorasi_cake_id',$decorId)->update([
            'flag_delete' => 1,
          ]);
          return redirect('/listdecorcake')->with('statushapus','Success');
    }
      }
  	}
}
