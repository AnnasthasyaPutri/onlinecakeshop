<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Notif;
use App\Model\Password_Reset;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use App\Model\Product;
use App\Model\Kustom;
use Response;
use Hash;
use DB;
use Crypt;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;
use Auth;
use DateTime;
use \Cart as Cart;

class CartController extends Controller
{
  	public function index(){
		return view('User.viewcart');
	}

	public function store(Request $request){
		// dd($request);
		$base 	= BaseCake::where('base_cake_id',$request->base)->first();
		$cover 	= CoverCake::where('cover_cake_id',$request->cover)->first();
		$dekor 	= DekorasiCake::where('dekorasi_cake_id',$request->decor)->first();
		
		foreach(Cart::content() as $item){
			if($item->id === $request->base && $item->options->cover === $request->cover && $item->options->dekor === $request->decor){

				$qty = $item->qty + $request->qty;
				Cart::update($item->rowId, ['qty' => $qty]);
				return redirect('/cart')->with('statusok','Success');
			}
		}

		if($request->product_id != NULL){
			$name = Product::where('product_id',$request->product_id)->value('product_name');
			$image = 'product/'.Product::where('product_id',$request->product_id)->value('product_image');
			$product = $request->product_id;
			$kustom = 0;
		}
		else{
			$name = "Custom";
			$custom = Kustom::where('kustom_id',$request->kustom_id)->first();
			// dd($custom);
			$image = 'custom/'.$custom->image;
			$product = 0;
			$kustom = $custom->kustom_id;
		}

		$base = BaseCake::where('base_cake_id',$request->base)->first();
		if($base->size_cake == "d22"){
			$size = "Diameter 22";
		}
		else{
			$size = "Diameter 18";
		}

        Cart::add($base->base_cake_id,$name,1,$request->price,['base_name'=>$base->base_cake_name,'cover'=>$request->cover,'cover_name'=>$cover->cover_cake_name,'dekor'=>$request->decor,'dekor_name'=>$dekor->dekorasi_cake_name,'produk'=>$product,'kustom'=>$kustom,'calorie'=>$request->calorie,'image'=>$image,'note'=>"",'size'=>$size])->associate('\App\Model\BaseCake','App\Model\CoverCake','App\Model\DekorasiCake');
        return redirect('/cart')->with('statusok','Success');
	}

	public function update(Request $request)
    {
    	$id = $request->id;
        $validator = Validator::make($request->all(), [
            'qty' => 'required|numeric',
        ]);

        if ($validator->fails()) {
        	$message = "Quantity tidak sesuai";
            session()->flash('error_message', 'Error updating quantity');
            return response()->json(['success' => false, 'message' => $message]);
        }
        else{
			
			Cart::update($id, ['qty' => $request->qty]);
			session()->flash('success_message', 'Quantity was updated successfully!');

			$message = "Quantity berhasil diubah!";
			return response()->json(['success' => true, 'message' => $message, 'rowid' => $id, 'qty' => $request->qty]);
		}
    }

    public function updatenote(Request $request)
    {
    	$id = $request->id;
        $validator = Validator::make($request->all(), [
            'note' => 'required',
        ]);

        if ($validator->fails()) {
        	$message = "Note tidak sesuai";
            session()->flash('error_message', 'Error updating quantity');
            return response()->json(['success' => false, 'message' => $message]);
        }
        else{
        	$pc = Cart::get($id);
    		$option = $pc->options->merge(['note'=>$request->note]);
			Cart::update($id, ['options' => $option]);
			session()->flash('success_message', 'Note was updated successfully!');

			$message = "Note berhasil diubah!";
			return response()->json(['success' => true, 'message' => $message, 'rowid' => $id, 'note' => $request->note]);
		}
    }

	public function destroy($id)
    {
        Cart::remove($id);
        return redirect('cart')->with('statushapus','Success');

    }

    public function emptyCart()
    {
        Cart::destroy();
        return redirect('cart')->with('statushapus','Success');
    }
}
