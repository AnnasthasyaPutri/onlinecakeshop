<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DetailInformation;

class InformationController extends Controller
{
  	public function index(){
  		$info = DetailInformation::where('information_id','1')->first();
  		return view('User.viewinformation',compact('info'));
  	}
}