<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VoucherUser;
use App\Model\User;
use App\Model\Voucher;
use \Cart as Cart;
use Auth;
use Crypt;

class YourVoucherController extends Controller
{
  	public function index(){
  		$vchrs = VoucherUser::where('user_id',Auth::User()->user_id)->get();
  		foreach ($vchrs as $vchr) {
  			if($vchr->voucher->expired_date < \Carbon\Carbon::now()->toDateString()){
  				$update = VoucherUser::where('voucher_user_id',$vchr->voucher_user_id)->update([
  					'flag_delete'=> 1,
  				]);
  			}
  			else{

  			}
  		}

  		$vouchers = VoucherUser::where('user_id',Auth::User()->user_id)->where('flag_delete','0')->get();
  		// dd($vouchers);
  		return view('User.yourvoucher',compact('vouchers'));
  	}

  	public function viewreedem(){
  		$date = date('d-m-Y');
  		$vchrs = Voucher::where('flag_delete',0)->get();
  		foreach ($vchrs as $vchr) {
  			if($vchr->expired_date < $date){
  				$update = Voucher::where('voucher_id',$vchr->voucher_id)->update([
  					'flag_delete'=> 1,
  				]);
  			}
  			else{

  			}
  		}

  		$vouchers = Voucher::where('qty','!=',0)->where('expired_date','>',$date)->where('flag_delete',0)->get();
  		// dd($vouchers);
  		return view('User.tukarpoin',compact('vouchers'));
  	}

  	public function reedem($id){
  		$voucher_id = Crypt::decrypt($id);
  		$voucher = Voucher::where('voucher_id',$voucher_id)->first();
  		if($voucher->point <= Auth::User()->point){
  			$newpoin = Auth::User()->point - $voucher->point;
  
  			$vuser = new VoucherUser;
  			$vuser->user_id = Auth::User()->user_id;
  			$vuser->voucher_id = $voucher_id;
  			$vuser->save();

  			$poin = User::where('user_id',Auth::User()->user_id)->update([
  				'point' => $newpoin,
  			]);

  			$qty = $voucher->qty - 1;
  			$vchr = Voucher::where('voucher_id',$voucher_id)->update([
  				'qty' => $qty,
  			]);
  			return redirect('/yourvoucher');
  		}
  		else{
  			return redirect('/reedemvoucher')->with('fail','Danger');
  		}
  	}

  	public function viewaddvoucher($id){
  		$date = date('d-m-Y');
  		$vchrs = VoucherUser::where('user_id',Auth::User()->user_id)->get();
  		foreach ($vchrs as $vchr) {
  			if($vchr->voucher->expired_date < $date){
  				$update = VoucherUser::where('voucher_user_id',$vchr->voucher_user_id)->update([
  					'flag_delete'=> 1,
  				]);
  			}
  			else{

  			}
  		}

  		$vouchers = VoucherUser::where('user_id',Auth::User()->user_id)->where('flag_delete','0')->get();
  		$vchr_id = Crypt::decrypt($id);
  		return view('User.usevoucher',compact('vouchers','vchr_id'));
  	}

  	public function usevoucher(Request $request){

  		if(Auth::check()==true){
			$vouchers = VoucherUser::where('user_id',Auth::User()->user_id)->get();
		}
		else{
			$vouchers = null;
		}

  		$total = 0;
  		$flag = 0;

		foreach (Cart::content() as $item) {
			$total = $total + ($item->price * $item->qty);
		}

  		if($request->voucher == 0){
  			
  			$potongan = 0;
  			$vchr = 0;
  			$message = "Kupon batal digunakan";
  			return view('User.checkout',compact('message','vchr','potongan','total','vouchers'));
  		}
  		else{
  			$voucher = Voucher::where('voucher_id',$request->voucher)->first();
  			// dd($voucher);

			if($voucher->voucher_type == 5){
				if($total < $voucher->minimum){
					$potongan = 0;
					$message = "Ketentuan kupon tidak sesuai";
					$vchr = 0;
					return view('User.checkout',compact('message','vchr','potongan','total','vouchers'));
				}
				else{
					if($voucher->discount != NULL && $voucher->percent != NULL){
						$diskon = $total*$voucher->percent;
						if($diskon > $voucher->discount){
							$potongan = $voucher->discount;
						}
						else{
							$potongan = $diskon;
						}
					}
					elseif ($voucher->discount != NULL && $voucher->percent == NULL){
						$potongan = $voucher->discount;
					}
					else{
						$potongan = $total*$voucher->percent;
					}
					$total = $total - $potongan;
					$message = "Kupon berhasil digunakan";
					$vchr = $voucher->voucher_id;
					return view('User.checkout',compact('message','vchr','potongan','total','vouchers'));
				}
			}
			else{
				foreach (Cart::content() as $item) {
					if($voucher->voucher_type == 1){
						if($voucher->base_cake_id == $item->id){
							$flag = $flag+1;
						}
						else{
							$flag = $flag;
						}
					}
					elseif($voucher->voucher_type ==2){
						if($voucher->cover_cake_id == $item->options->cover) {
							$flag = $flag+1;
						}
						else{
							$flag = $flag;
						}
					}
					elseif($voucher->voucher_type ==3){
						if($voucher->decor_cake_id == $item->options->dekor) {
							$flag = $flag+1;
						}
						else{
							$flag = $flag;
						}
					}
					else{
						if($voucher->product_id == $item->options->produk){
							$flag = $flag+1;
						}
						else{
							$flag = $flag;
						}
					}
				}

				if($flag < $voucher->minimum){
					$potongan = 0;
					$message = "Ketentuan kupon tidak sesuai";
					$vchr = 0;
					return view('User.checkout',compact('message','vchr','potongan','total','vouchers'));
				}
				else{
					if($voucher->discount != NULL && $voucher->percent != NULL){
						$diskon = $total*$voucher->percent;
						if($diskon > $voucher->discount){
							$potongan = $voucher->discount;
						}
						else{
							$potongan = $diskon;
						}
					}
					elseif ($voucher->discount != NULL && $voucher->percent == NULL){
						$potongan = $voucher->discount;
					}
					else{
						$potongan = $total*$voucher->percent;
					}
					$total = $total - $potongan;
					$message = "Kupon berhasil digunakan";
					$vchr = $voucher->voucher_id;
					return view('User.checkout',compact('message','vchr','potongan','total','vouchers'));
				}
			}
  		}
  		// dd($request, $message, $vchr, $potongan, $total, $vouchers);
  		return view('User.checkout',compact('message','vchr','potongan','total','vouchers'));
  	}

  	public function unusevoucher(){
  		$total = 0;
		foreach (Cart::content() as $item) {
			$total = $total + ($item->price*$item->qty);
		}
		$vchr = 0;
		$potongan = 0;
		return view('User.viewcart',compact('total','vchr','potongan','message'));
  	}
}
