<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Notif;
use Crypt;
use \Cart as Cart;

class ProductController extends Controller
{
  	public function viewd22(){
  		$products = Product::where('size_cake','d22')->where('flag_delete',0)->get();
  		return view('User.product22',compact('products'));
  	}

  	public function viewd18(){
  		$products = Product::where('size_cake','d18')->where('flag_delete',0)->get();
  		return view('User.product18',compact('products'));
  	}

  	public function viewdetailproduct($id){
  		$productid = Crypt::decrypt($id);
  		$product = Product::where('product_id',$productid)->first();
  		return view('User.viewdetailproduct',compact('product'));
  	}
}
