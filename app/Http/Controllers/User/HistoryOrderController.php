<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\HeaderTransaction;
use App\Model\DetailTransaction;
use App\Model\DekorasiCake;
use App\Model\CoverCake;
use App\Model\BaseCake;
use App\Model\Product;
use App\Model\User;
use \Cart as Cart;
use Auth;
use Crypt;

class HistoryOrderController extends Controller
{
  	public function index(){
  		$orders = HeaderTransaction::orderBy('transaction_header_id','desc')->where('user_id',Auth::User()->user_id)->get();

  		return view('User.historyorder',compact('orders'));
  	}

  	public function viewdetail($id){
  		$trsc_id = Crypt::decrypt($id);
  		$orders = DetailTransaction::where('transaction_header_id',$trsc_id)->get();
  		// dd($orders, $trsc_id);

  		return view('User.detailorder',compact('orders'));
  	}
}
