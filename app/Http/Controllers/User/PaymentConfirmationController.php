<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\HeaderTransaction;
use App\Model\DataPembayaran;
use App\Model\Notif;
use Response;
use Hash;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;

class PaymentConfirmationController extends Controller
{
  	public function view(){
	  	return view('User.paymentconfirmation');
	}

	public function confirmation(Request $request){
		$message = [
    		'kode_pemesanan.required'	=>'Kode Pemesanan Tidak Boleh Kosong',
            'kode_pemesanan.exists'		=>'Kode Pemesanan anda tidak terdaftar',
    		'name.required'				=>'Password tidak boleh kosong',
            'jumlah.required'			=>'Password tidak boleh kosong',
            'bank.required'				=>'Bank Asal tidak boleh kosong',
            'tanggal.required'			=>'Tanggal Pembayaran tidak boleh kosong'
    	];

    	$Validator = Validator::make($request->all(),[
    		'kode_pemesanan'	=>'required|exists:mstransaction_header,transaction_code',
    		'name'				=>'required',
    		'jumlah'			=>'required',
    		'bank'				=>'required',
    		'tanggal'			=>'required',
    	],$message);

        $imagevalidator = Validator::make($request->all(),[
            'image' => 'mimes:jpeg,jpg,png,gif|max:25000',
        ],$message);

    	if($Validator->passes()){
            $filename   = "";
            $path       = 'Payment/';
            $image      = $request->file('image');
            if($request->hasfile('image')){
                if($imagevalidator->fails()){
                    return redirect()->back()->withErrors($imagevalidator)->withInput();
                }
                else{
                    $filename   = "BP-".$request->kode_pemesanan;
                    $image->move($path, $filename);

                    $transaksi = HeaderTransaction::where('transaction_code',$request->kode_pemesanan)->first();
                    $transaksi->payment_status = '1';
                    $transaksi->transaction_status = 1;
                    $transaksi->save();

                    $pembayaran = new DataPembayaran;
                    $pembayaran->transaction_header_id  = $transaksi->transaction_header_id;
                    $pembayaran->tanggal_pembayaran     = $request->tanggal;
                    $pembayaran->jumlah_pembayaran      = $request->jumlah;
                    $pembayaran->bank_asal              = $request->bank;
                    $pembayaran->atas_nama              = $request->name;
                    $pembayaran->bukti_pembayaran       = $filename;
                    $pembayaran->save();

                    if($transaksi->user_id != null){
                        $data = [];

                        try{
                          $data['status'] = "Pesanan kamu dengan kode ".$transaksi->transaction_code." sudah dibayar. Silahkan lihat perkembangannya ";
                          $data['name'] = $transaksi->user_name;
                          $email = $transaksi->user_email; 
                          $data['path']= url('/historyorder');

                          Mail::send('Mail.notification', $data, function ($message) use($email) {
                                    $message->to($email)->subject('Berhasil melakukan pembayaran');
                          });
                        }
                        catch(Exception $e){
                        }

                        $notif = new Notif;
                        $notif->user_id = $transaksi->user_id;
                        $notif->subject = "Pesanan ".$transaksi->transaction_code;
                        $notif->keterangan = "Sudah melakukan konfirmasi pembayaran";
                        $notif->tanggal = \Carbon\Carbon::now();
                        $notif->save();
                    }
                    else{
                        $data = [];

                        try{
                          $data['status'] = "Pesanan kamu dengan kode ".$transaksi->transaction_code." sudah dibayar.";
                          $email = $transaksi->user_email;

                          Mail::send('Mail.guest', $data, function ($message) use($email) {
                                    $message->to($email)->subject('Berhasil melakukan pembayaran');
                          });
                        }
                        catch(Exception $e){
                        }
                    }

                    return redirect('/paymentconfirmation')->with('status','Success');
                }
            }
    		else{
                return redirect()->back()->withErrors($imagevalidator)->withInput();
            }
    	}
    	else{
    		return redirect()->back()->withErrors($Validator)->withInput();
  		}
	}
}
