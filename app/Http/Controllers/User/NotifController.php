<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notif;
use Auth;
use Crypt;

class NotifController extends Controller
{
  	public function index(){
  		$notifs = Notif::where('user_id',Auth::User()->user_id)->get();
  		return view('User.notif',compact('notifs'));
  	}

  	public function delete($id){
  		$notifId = Crypt::decrypt($id);
    	$notif=Notif::where('notif_id',$notifId)->forcedelete();
    	return redirect('/viewnotif');
    }

    public function empty(){
    	$notif=Notif::where('user_id',Auth::User()->user_id)->forcedelete();
    	return redirect('/viewnotif');
    }
}