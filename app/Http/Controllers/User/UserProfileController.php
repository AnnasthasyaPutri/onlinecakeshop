<?php

namespace App\Http\Controllers\User;
use App\Model\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Hash;
use \Cart as Cart;

class UserProfileController extends Controller
{
    public function index(){
        $user = User::where('user_id',Auth::User()->user_id)->first();
        return view('User.profile',compact('user'));
    }

    public function ubahprofile(){
        $user = User::where('user_id',Auth::User()->user_id)->first();
        return view('User.ubahprofile',compact('user'));
    }
    
    public function ubah(Request $request){
        $message = [
            'name.required'     =>'User Name Tidak Boleh Kosong',
            'email.required'    =>'Email Tidak Boleh Kosong',
            'email.email'       =>'Format Email Salah',
            'phone.required'    =>'No Telp tidak boleh kosong',
            'phone.numeric'     =>'No telp harus angka',
            'phone.between'     =>'No telp harus di antar 10 dan 13',
        ];

        $Validator = Validator::make($request->all(),[
            'name'  =>'required',
            'email' =>'required|email',
            'phone' =>'required|between:9,14',
            'phone' =>'required|numeric'
        ],$message);

        if($Validator->passes()){
            $data = User::where('user_id',Auth::User()->user_id)->first();
            $data->user_name = $request->name;
            $data->user_email = $request->email;
            $data->user_phone = $request->phone;
            $data->save();

            return redirect('/profile');
        }
        else
        {
        return redirect()->back()->withErrors($Validator)->withInput();
        }
    }

    public function ubahsandi(Request $request){
        $message = [
            'password.required'                 =>'Password tidak boleh kosong',
            'password.min'                      =>'Password minimal 6 karakter',
            'password.confirmed'                =>'Password dan Confirm Password Tidak sama',
            'password_confirmation.required'    =>'Password tidak boleh kosong',
            'password_confirmation.min'         =>'Password Minimal 6 karakter'
        ];

        $Validator = Validator::make($request->all(),[
            'password'              =>'required|min:6|confirmed',
            'password_confirmation' =>'required|min:6'
        ],$message);

        if($Validator->passes()){
            $data = User::where('user_id',Auth::User()->user_id)->first();
            $data->user_password = Hash::make($request->password);
            $data->save();

          return redirect('/profile');
        }
        else{
            return redirect()->back()->withErrors($Validator)->withInput();
        }
    }
}
