<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Notif;
use App\Model\DetailTransaction;
use App\Model\HeaderTransaction;
use App\Model\VoucherUser;
use App\Model\BaseCake;
use App\Model\CoverCake;
use App\Model\DekorasiCake;
use App\Model\Product;
use Response;
use Hash;
use DB;
use Crypt;
use Illuminate\Support\Facades\Mail;
use Validator;
use Session;
use Auth;
use DateTime;
use \Cart as Cart;

class CheckoutController extends Controller
{
	public function index(){
		// dd(Cart::content());
		if(Cart::content()==null){
			return redirect('/productdiameter22');
		}
		else{
			$total = 0;
			foreach (Cart::content() as $item) {
				$total = $total + ($item->price*$item->qty);
			}
			$vchr = 0;
			$potongan = 0;
			$message = "";

			if(Auth::check()==true){
				$vouchers = VoucherUser::where('user_id',Auth::User()->user_id)->where('flag_delete',0)->get();
			}
			else{
				$vouchers = null;
			}
			return view('User.checkout',compact('total','vchr','potongan','message','vouchers'));
		}
	}

	public function store(Request $request){
		// dd($request);
		$message = [
            'name.required'		=>'Nama tidak boleh kosong',
  			'email.required'	=>'Email tidak boleh kosong',
  			'email.email'		=>'Format email salah',
      		'nomor_hp.required'	=>'Nomor HP tidak boleh kosong',
      		'nomor_hp.numeric'	=>'Nomor HP harus angka',
      		'nomor_hp.between'	=>'Nomor HP harus di antara 10 dan 13',
      		'tanggal.required'	=> 'Tanggal Pengambilan tidak boleh kosong',
        ];

        $Validator = Validator::make($request->all(),[
	  		'name'		=>'required',
	  		'email'		=>'required|email',
	      	'nomor_hp'	=>'required|numeric',
	      	'nomor_hp'	=>'required|between:9,14',
	      	'tanggal'	=>'required',
	  	],$message);

	  	if($Validator->passes()){
	  		$transaction = HeaderTransaction::orderBy('transaction_header_id','desc')->first();

		    $newid = '';
		    $code = 'SCO-';
		    $date = date('d');
		    $month = date('m');
		    $year = date('y');
		    $minute =date('i');
		    $hour =date('H');

		    $currdate = date('ymd');

		    if($transaction == null){
		      $id = 1;
		    }
		    else{
		      $id = $transaction->transaction_header_id+1;
		    }

		    $newid = $code.$year.$month.$date.$hour.$minute.$id;

		    if(Auth::check()==true){
		    	$user = Auth::User()->user_id;

		    	$notif = new Notif;
		    	$notif->user_id = $user;
		    	$notif->subject = "Pesanan ".$newid;
		    	$notif->keterangan = "Belum melakukan konfirmasi pembayaran";
		    	$notif->tanggal = \Carbon\Carbon::now();
		    	$notif->save();
		    }
		    else{
		    	$user = 0;
		    }

		    $header = new HeaderTransaction;
		    $header->user_id = $user;
		    $header->user_name = $request->name;
		    $header->user_email = $request->email;
		    $header->user_phone = $request->nomor_hp;
		    $header->transaction_code=$newid;
		    $header->total_price = $request->price;
		    $header->payment_status = 0;
		    $header->transaction_status = 0;
		    $header->pick_date = $request->tanggal;
		    $header->save();

		    foreach (Cart::content() as $item) {
		      $detail = new DetailTransaction;
		      $detail->transaction_header_id = $header->transaction_header_id;
		      $detail->product_name = $item->name;
		      $detail->size_cake = $item->options->size;
		      $detail->price = $item->price;
		      $detail->calorie = $item->options->calorie;
		      $detail->qty = $item->qty;
		      $detail->pic = $item->options->image;
		      $detail->note = $item->options->note;
		      $detail->product_id = $item->options->produk;
		      $detail->base_cake_id = $item->id;
		      $detail->cover_cake_id = $item->options->cover;
		      $detail->dekorasi_cake_id = $item->options->dekor;
		      $detail->save();

		      $base = BaseCake::where('base_cake_id',$item->id)->first();
		      $base->flag_populer = $base->flag_populer+1;
		      $base->save();

		      $cover = CoverCake::where('cover_cake_id',$item->options->cover)->first();
		      $cover->flag_populer = $cover->flag_populer+1;
		      $cover->save();

		      $dekor = DekorasiCake::where('dekorasi_cake_id',$item->options->dekor)->first();
		      $dekor->flag_populer = $dekor->flag_populer+1;
		      $dekor->save();

		      if($item->options->produk != 0){
		      	$produk = Product::where('product_id',$item->options->produk)->first();
		      	$produk->flag_populer = $produk->flag_populer+1;
		      	$produk->save();
		      }
		      else{

		      }

		    }

		    if($request->voucher != 0){
		    	$voucher = VoucherUser::where('voucher_id',$request->voucher)->where('flag_delete',0)->first();
		    	// dd($request);
		    	$voucher->flag_delete = 1;
		    	$voucher->save();
		    }
		    else{
		    	
		    }

	        Cart::destroy();
	        $data = [];

	        try{
	          $data['kode'] = $newid;
	          $email = $request->email;
	          $data['path']= url('/paymentconfirmation');

	          Mail::send('Mail.order', $data, function ($message) use($email) {
	                    $message->to($email)->subject('Pesanan berhasil dibuat');
	          });
	        }
	        catch(Exception $e){
	        }

			return redirect('/productdiameter22')->with('status2','Success');
	  	}
	  	else{
	  		return redirect()->back()->with('statusfail','Danger')->withErrors($Validator)->withInput();
	  	}
	}
}